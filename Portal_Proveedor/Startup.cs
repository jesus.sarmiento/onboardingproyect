﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Portal_Proveedor.Startup))]
namespace Portal_Proveedor
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
