﻿using Portal_Proveedor.Models.Correo;
using Portal_Proveedor.Models.Registro;
using Portal_Proveedor.Services.Utility;
using System;
using System.Web.Mvc;
using System.Web.Security;

namespace Portal_Proveedor.Controllers
{
    public class RegistroController : Controller
    {
        private RegistroDM rdm = new RegistroDM();

        [HttpGet]
        public ActionResult Registrarse()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registrarse(Registro model)
        {
            ActionResult retorno = null;

            if (ModelState.IsValid)
            {
                try
                {
                    var Existe = Membership.GetUser(model.UserName);
                    if (Existe == null)
                    {
                        if (!rdm.ExisteRTN(model.RTN))
                        {
                            if (model.clave.Equals(model.confirmaClave))
                            {
                                model = rdm.ValidarProveedor(model);

                                model.activo = true;

                                if (model.activo)
                                {
                                    Membership.CreateUser(model.UserName, model.clave, model.correo);
                                    if (Membership.GetUser(model.UserName).ToString() != null)
                                    {
                                        MembershipUser user = Membership.GetUser(model.UserName);
                                        user.Comment = model.nombreEmpresa;
                                        Membership.UpdateUser(user);

                                        int res = rdm.RegistrarUsuario(model);
                                        if (res == 0)
                                        {
                                            Roles.AddUserToRole(model.UserName, "usuario");
                                            Membership.ValidateUser(model.UserName, model.clave);
                                            FormsAuthentication.SetAuthCookie(model.UserName, false);
                                            rdm.AgregarInicioSesion(model.UserName);
                                            //enviar correo
                                            CorreoElectronicoDM cedm = new CorreoElectronicoDM();
                                            cedm.EnviarCorreo(model.correo, "", 1);
                                            //retorno = RedirectToAction("Index", "Home");
                                            retorno = RedirectToAction("RegistroFacturas", "OnBase");

                                        }
                                        else
                                        {
                                            //Si falla
                                        }
                                    }
                                }
                                else
                                {
                                    switch (model.codeError)
                                    {
                                        case -1:
                                            ModelState.AddModelError("UserName", "El proveedor se encuentra bloqueado, por favor verifíquelos o comuníquese con el administrador.");
                                            break;
                                        case -2:
                                            ModelState.AddModelError("UserName", "El proveedor no existe en el sistema, por favor verifíquelos o comuníquese con el administrador.");
                                            break;
                                        default:
                                            ModelState.AddModelError("UserName", "Los datos del proveedor no coinciden, por favor verifíquelos o comuníquese con el administrador.");
                                            break;
                                    }

                                    retorno = View("Registrarse", model);
                                }

                            }
                            else
                            {
                                ModelState.AddModelError("UserName", "Las claves no coinciden, por favor ingréselas de nuevo.");
                                retorno = View("Registrarse", model);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("UserName", "El RTN indicado ya se encuentra registrado, verifique la información ingresada.");
                            retorno = View("Registrarse", model);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("UserName", "El proveedor ya se encuentra registrado, para ingresar debe iniciar sesión.");
                        retorno = View("Registrarse", model);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("UserName", "Ha ocurrido un error inesperado, por favor comuníquese con el administrador. " + ex.Message);
                    retorno = View("Registrarse", model);
                    LoggerManager.GetInatance().Error($"Failed method Registrarse in RegistroContoller, System message: {ex}");
                }
            }
            else
            {
                ModelState.AddModelError("UserName", "Debe ingresar todos los datos para proceder .");
                retorno = View("Registrarse", model);
            }

            return retorno;
        }

        [HttpGet]
        public ActionResult InicioSesion()
        {
            if (User.Identity.Name != null && User.Identity.Name !="")
            {
                if (Roles.IsUserInRole(User.Identity.Name, "admin"))
                {
                    return RedirectToAction("ReporteInicioSesion", "Administracion");
                }
                else
                {
                    return RedirectToAction("RegistroFacturas", "OnBase");
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult InicioSesion(Registro model)
        {
            ActionResult retorno = null;
            string resultado = null;

            try
            {
                if (model.UserName != null)
                {
                    var Existe = Membership.GetUser(model.UserName);
                    if (Existe != null)
                    {
                        if (model.clave != null)
                        {
                            MembershipUser user = Membership.GetUser(model.UserName);
                            //if (!rdm.RequiereCambioClave(model.UserName))
                            //{
                            if (model.UserName == "admin" && Membership.ValidateUser(model.UserName, model.clave))
                            {
                                if (user.IsLockedOut)
                                    user.UnlockUser();
                                FormsAuthentication.SetAuthCookie(model.UserName, false);
                                rdm.AgregarInicioSesion(model.UserName);
                                if (HttpContext.Session["returl"] as String != null && Url.IsLocalUrl(HttpContext.Session["returl"] as String))
                                {
                                    return Redirect(HttpContext.Session["returl"] as String);
                                }
                                //retorno = RedirectToAction("Index", "Home");
                                if (Roles.IsUserInRole(user.UserName, "admin"))
                                {
                                    retorno = RedirectToAction("ReporteInicioSesion", "Administracion");
                                }
                                else
                                {
                                    retorno = RedirectToAction("RegistroFacturas", "OnBase");
                                }
                            }
                            else if (rdm.ProveedorBloqueado(model))//!user.IsApproved)
                            {
                                if (!user.IsLockedOut)
                                    for (int i = 0; i < Membership.MaxInvalidPasswordAttempts; i++)
                                        Membership.ValidateUser(user.UserName, "passwordError");
                                resultado = "En este momento no se puede ingresar con el usuario indicado porque se encuentra bloqueado, por favor comuníquese con el administrador del sistema";
                                ModelState.AddModelError("UserName", resultado);
                                retorno = View("InicioSesion", model);
                            }
                            else if (Membership.ValidateUser(model.UserName, model.clave))
                            {
                                if (user.IsLockedOut)
                                    user.UnlockUser();
                                FormsAuthentication.SetAuthCookie(model.UserName, false);
                                rdm.AgregarInicioSesion(model.UserName);
                                if (HttpContext.Session["returl"] as String != null && Url.IsLocalUrl(HttpContext.Session["returl"] as String))
                                {
                                    return Redirect(HttpContext.Session["returl"] as String);
                                }
                                //retorno = RedirectToAction("Index", "Home");
                                if (Roles.IsUserInRole(user.UserName, "admin"))
                                {
                                    retorno = RedirectToAction("ReporteInicioSesion", "Administracion");
                                }
                                else
                                {
                                    retorno = RedirectToAction("RegistroFacturas", "OnBase");
                                }
                            }
                            else
                            {
                                resultado = "Verifique los datos ingresados";
                                ModelState.AddModelError("UserName", resultado);
                                retorno = View("InicioSesion", model);
                            }
                            //}
                            //else
                            //{
                            //    retorno = RedirectToAction("CambiarClave", model.UserName);
                            //}
                        }
                        else
                        {
                            resultado = "Debe ingresar el usuario y la contraseña";
                            ModelState.AddModelError("UserName", resultado);
                            retorno = View("InicioSesion", model);
                        }
                    }
                    else
                    {
                        resultado = "Debe registrarse en el portal.";
                        ModelState.AddModelError("UserName", resultado);
                        retorno = View("Registrarse", model);
                    }
                }
                else
                {
                    resultado = "Debe ingresar el usuario y la contraseña";
                    ModelState.AddModelError("UserName", resultado);
                    retorno = View("Registrarse", model);
                }
            }
            catch (Exception ex)
            {
                resultado = ex.Message;
                ModelState.AddModelError("UserName", resultado);
                retorno = View("Registrarse", model);
                LoggerManager.GetInatance().Error($"Failed method Registrarse in RegistroContoller, System message: {ex}");
            }
            return retorno;
        }

        public ActionResult CerrarSesion()
        {
            String Identificacion = User.Identity.Name;
            Session.Clear();
            FormsAuthentication.SignOut();
            //return RedirectToAction("Index", "Home");
            return RedirectToAction("InicioSesion", "Registro");
        }

        public ActionResult OlvidoClave()
        {

            return View();
        }

        //[ValidateAntiForgeryToken]
        public string RegenerarClave(Registro model)
        {
            string resultado = string.Empty;
            string id = string.Empty;
            var Existe = Membership.GetUser(model.UserName);

            if (Existe != null)
            {
                if (model.correo != Existe.Email)
                {
                    resultado = "El correo electrónico no coincide con el registrado. Por favor valide la información.";
                }
                else
                {
                    MembershipUser user = Membership.GetUser(model.UserName);
                    if (user.IsLockedOut)
                        user.UnlockUser();
                    string clave = user.ResetPassword();
                    id = rdm.RegenerarClave(model, clave);
                    resultado = "Se ha enviado un correo con las indicaciones para cambiar la clave.";
                }
            }
            else
            {
                resultado = "El usuario no existe. Por favor valide los datos.";
            }
            return resultado;
        }

        public ActionResult RecuperarClave(string id)
        {
            CambioClave model = new CambioClave();

            if (id != null)
            {
                model = rdm.ObtenerInfoCambioClave(id);
            }

            return View(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public string RecuperacionClave(CambioClave model)
        {
            string resultado = string.Empty;
            int id = 0;

            if (model.claveNueva.Equals(model.confirmacionClave))
            {
                id = rdm.ValidarClaveTempNoVencida(model.usuario);
                if (id > 0)
                {
                    MembershipUser user = Membership.GetUser(model.usuario);
                    if (user.ChangePassword(model.claveVieja, model.claveNueva))
                    {
                        rdm.MarcarClaveUsada(id);
                        //
                        Membership.ValidateUser(model.usuario, model.claveNueva);
                        FormsAuthentication.SetAuthCookie(model.usuario, false);
                        rdm.AgregarInicioSesion(model.usuario);

                        resultado = "Se ha cambiado la clave exitosamente.";
                    }
                    else
                    {
                        resultado = "No se ha podido cambiar la clave, por favor valide la información ingresada. Si el problema persiste, comuníquese con el administrador.";
                    }
                }
                else
                {
                    resultado = "Ha expirado el cambio de clave, por favor genere uno nuevo.";
                }
            }
            else
            {
                resultado = "La clave nueva y la confirmación no coinciden, por favor verifique los datos ingresados.";
            }

            return resultado;
        }

    }
}