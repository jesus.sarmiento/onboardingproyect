﻿using Portal_Proveedor.Models.Administracion;
using Portal_Proveedor.Models.Registro;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;

namespace Portal_Proveedor.Controllers
{
    public class AdministracionController : Controller
    {
        // GET: Administracion
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult ReporteInicioSesion()
        {
            return View();

        }

        [Authorize(Roles = "usuario")]
        public ActionResult CambiarClave()
        {
            return View();
        }

        [Authorize(Roles = "usuario")]
        //[ValidateAntiForgeryToken]
        public string CambiarLaClave(string claveVieja, string claveNueva)
        {
            string resultado = null;

            try
            {
                MembershipUser user = Membership.GetUser(User.Identity.Name);

                if (user.ChangePassword(claveVieja, claveNueva))
                {
                    resultado = "Ok";
                }
                else
                {
                    resultado = "Error";
                }
            }
            catch (Exception ex)
            {
                resultado = "Error";
            }

            return resultado;
        }

        [Authorize(Roles = "usuario")]
        public ActionResult Perfil()
        {
            AdministracionDM adm = new AdministracionDM();
            Registro proveedor = adm.ConsultarDatosDelProveedor(User.Identity.Name);
            return View(proveedor);
        }

        [Authorize(Roles = "admin")]
        public PartialViewResult InicioSesion()
        {
            RegistroDM rdm = new RegistroDM();
            List<UsuariosIngresados> lista = rdm.ListaDeUsuarios();
            return PartialView(lista);
        }

        [Authorize(Roles = "admin")]
        public PartialViewResult UsuariosBloqueados()
        {
            AdministracionDM adm = new AdministracionDM();
            List<UsuariosBloqueados> lista = adm.ReporteUsuariosBloqueados();
            return PartialView(lista);
        }

        [Authorize(Roles = "admin")]
        public PartialViewResult IntentosFallidos()
        {
            AdministracionDM adm = new AdministracionDM();
            List<IntentosFallidos> lista = adm.ReporteIntentosFallidos();
            return PartialView(lista);
        }

        public string DesbloquearUsuario(string id)
        {
            string resultado = "Usuario desbloqueado";
            if (id != null || id != "")
            {
                MembershipUser user = Membership.GetUser(id);
                if (user.IsLockedOut)
                    user.UnlockUser();
            }
            return resultado;
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult CambioClave()
        {
            Registro usuario = new Registro();
            return View(usuario);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult CambioClave(Registro usuario)
        {
            try
            {
                MembershipUser user = Membership.GetUser(usuario.UserName);

                if (user != null)
                {
                    if (user.IsLockedOut)
                        user.UnlockUser();
                    string clave = user.ResetPassword();
                    ModelState.AddModelError("usuario", "La nueva contraseña es: " + clave);
                }
                else
                {
                    //Usuario no existe
                    ModelState.AddModelError("msjError", "El usuario indicado no existe.");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("msjError", "Ha ocurrido el siguiente error: " + ex.Message);
            }
            return View(usuario);
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult CambioCorreo()
        {
            Registro usuario = new Registro();
            return View(usuario);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult CambioCorreo(Registro usuario)
        {



            try
            {
                MembershipUser user = Membership.GetUser(usuario.UserName);
                if (user != null)
                {
                    user.Email = usuario.correo;
                    Membership.UpdateUser(user);
                    ModelState.AddModelError("usuario", "Se ha modificado el correo exitosamente.");
                }
                else
                {
                    //Usuario no existe
                    ModelState.AddModelError("msjError", "El usuario indicado no existe.");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("msjError", "Ha ocurrido el siguiente error: " + ex.Message);
            }
            return View(usuario);
        }
    }
}

