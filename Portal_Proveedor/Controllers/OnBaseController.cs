﻿using Portal_Proveedor.Models.Administracion;
using Portal_Proveedor.Models.Correo;
using Portal_Proveedor.Models.OnBase;
using Portal_Proveedor.Models.Registro;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Portal_Proveedor.Controllers
{
    public class OnBaseController : Controller
    {
        private DocumentosDM ddm = new DocumentosDM();
        private CorreoElectronicoDM cdm = new CorreoElectronicoDM();

        // GET: OnBase
        public ActionResult Index()
        {
            return View();
        }

        #region Carga Individual de Facturas

        [Authorize(Roles = "usuario")]
        public ActionResult RegistroFacturas()
        {
            List<Documentos> lista = new List<Documentos>();

            lista = ddm.ConsultaOrdenesCompra(User.Identity.Name);

            return View(lista);
        }

        [Authorize(Roles = "usuario")]
        public ActionResult ImportarFactura(Documentos doc)
        {
            // Validar constancia de pago a cuenta
            doc.tieneConstancia = ddm.ConsultaConstanciaPagoCuenta(User.Identity.Name);
            return View(doc);
        }

        [Authorize(Roles = "usuario")]
        public ActionResult ImportaFactura(Documentos doc)
        {
            string email = Membership.GetUser().Email;
            doc.codigoProveedor = User.Identity.Name;
            long nuevoDoc = ddm.ImportarDocumentoPath(doc);

            if (nuevoDoc > 0)
            {
                ViewBag.Message = "La factura se ha almacenado correctamente.";
                cdm.EnviarCorreo(email, doc.numFiscal, 4);
            }
            else
            {
                switch (nuevoDoc)
                {
                    case -2:
                        ViewBag.Message = "El número de factura fue ingresado anteriormente, y se encuentra en revisión. Por favor verifique los datos ingresados.";
                        break;
                    case -3:
                        ViewBag.Message = "El monto de la Orden de Compra es menor del declarado en la factura. Por favor verifique los datos ingresados.";
                        break;
                    default:
                        ViewBag.Message = "No se ha almacenado la factura, inténtelo de nuevo. Si el problema persiste comuníquese con el administrador.";
                        break;
                }

            }

            return View(doc);
        }


        #endregion


        #region Carga Masiva de Facturas


        [Authorize(Roles = "usuario")]
        public ActionResult CargaMasivaFacturas()
        {
            List<Documentos> lista = new List<Documentos>();

            Documentos doc = new Documentos();

            lista = ddm.ConsultaOrdenesCompra(User.Identity.Name);
            if (lista.Count > 0) lista[0].tieneConstancia = ddm.ConsultaConstanciaPagoCuenta(User.Identity.Name);

            // lista = ddm.ConsultaDocumentos(User.Identity.Name);
            HttpContext.Session["listaPO"] = lista;
            return View(lista);
        }

        [Authorize(Roles = "usuario")]
        public ActionResult CargaDeFacturas(HttpPostedFileBase file, IEnumerable<HttpPostedFileBase> files)
        {
            List<ExcelResultado> lista = new List<ExcelResultado>();
            List<Documentos> listaPO = new List<Documentos>();

            if (HttpContext.Session["listaPO"] != null)
                listaPO = HttpContext.Session["listaPO"] as List<Documentos>;
            try
            {
                lista = ddm.CargarMasivamenteDocumentos(file, listaPO, User.Identity.Name, files);

                string email = Membership.GetUser().Email;
                cdm.EnviarCorreo(email, lista, 5);
            }
            catch (Exception ex)
            {
                ExcelResultado docs = new ExcelResultado();
                docs.consecutivo = 1;
                docs.Estado = "Ha ocurrido un error. Verifique la información ingresada en el Excel.";
                lista.Add(docs);
            }

            return View(lista);
        }

        #endregion


        #region Constancia de pago a cuenta

        [HttpGet]
        [Authorize(Roles = "usuario")]
        public ActionResult ImportarConstanciaCuenta()
        {
            AdministracionDM adm = new AdministracionDM();
            Registro proveedor = adm.ConsultarDatosDelProveedor(User.Identity.Name);
            Documentos doc = new Documentos();
            doc.codigoProveedor = User.Identity.Name;
            doc.rtn = proveedor.RTN;

            return View(doc);
        }

        [HttpPost]
        [Authorize(Roles = "usuario")]
        public ActionResult ImportarConstanciaCuenta(Documentos doc)
        {
            long nuevoDoc = ddm.ImportarDocumentoConstancia(doc);
            string email = Membership.GetUser().Email;
            if (nuevoDoc > 0)
            {
                ModelState.AddModelError("tieneConstancia", "La constancia de pago a cuenta se ha almacenado correctamente.");
                cdm.EnviarCorreo(email, doc.fechaDeVencimiento, 3);
            }
            else
            {
                ModelState.AddModelError("estado", "Ha ocurrido un error al importar el documento. Inténtelo nuevamente, si el problema persiste comuníquese con el administrador.");
            }

            return View("ImportarConstanciaCuenta", doc);
        }


        [Authorize(Roles = "usuario")]
        public string ImportaConstanciaCuenta(Documentos doc)
        {
            long nuevoDoc = ddm.ImportarDocumentoConstancia(doc);
            string resultado = "";
            if (nuevoDoc > 0)
            {
                resultado = "La constancia de pago a cuenta se ha almacenado correctamente.";
            }
            else
            {
                resultado = "Ha ocurrido un error al importar el documento. Inténtelo nuevamente, si el problema persiste comuníquese con el administrador.";
            }



            return resultado;
        }

        #endregion

        public ActionResult Prueba()
        {
            return View();
        }

        [Authorize(Roles = "usuario")]
        public ActionResult ConsultaFacturacion()
        {
            List<Documentos> lista = ddm.ConsultarDocumentosPOyFacturas(User.Identity.Name);
            return View(lista);
        }


        public string ConsultarDoc(long id)
        {
            string doc = string.Empty;

            id = 18058;

            doc = ddm.ConsultarDocumento(id, User.Identity.Name);

            return doc;
        }

        #region Estado de Cuenta


        [Authorize(Roles = "usuario")]
        public ActionResult EstadoCuenta()
        {

            List<Documentos> doc = ddm.ConsultarEstadoCuenta(User.Identity.Name);

            return View(doc);
        }

        [Authorize(Roles = "usuario")]
        public ActionResult EstadoCuentaConFechas()
        {
            return View();
        }

        public PartialViewResult EstadoDeCuenta(string id)/*DateTime fechaInicio, DateTime fechaFin)*/
        {
            DateTime fechaInicio = DateTime.Now.AddDays(-30);
            DateTime fechaFin = DateTime.Now;

            if (id == null && HttpContext.Session["ID"] != null) id = HttpContext.Session["ID"].ToString();

            if (id != null)
            {
                HttpContext.Session["ID"] = id;
                string[] lista = id.Split('?');
                fechaInicio = DateTime.Parse(lista[0]);
                fechaFin = DateTime.Parse(lista[1]).AddDays(1);
            }

            List<Documentos> doc = ddm.ConsultarEstadoCuentaConFecha(User.Identity.Name, fechaInicio, fechaFin);

            return PartialView(doc);
        }


        [Authorize(Roles = "usuario")]
        public ActionResult EstadoCuentaImpresion()
        {
            List<Documentos> doc = ddm.ConsultarEstadoCuenta(User.Identity.Name);
            if (doc.Count > 0)
            {
                MembershipUser user = Membership.GetUser(User.Identity.Name);
                doc[0].rtn = user.Comment;
            }
            return PartialView(doc);
        }

        [Authorize(Roles = "usuario")]
        public ActionResult ImprimirEstadoCuenta()
        {
            var report = new Rotativa.MVC.ActionAsPdf("EstadoCuentaImpresion");
            //{
            //    FileName = "Estado de cuenta -Portal de proveedores MQA.pdf"
            //};

            //return new Rotativa.AspNetCore.ViewAsPdf("DemoPageSizePDF")
            //{
            //    PageSize = Rotativa.AspNetCore.Options.Size.A6,
            //};

            return report;

        }

        [Authorize(Roles = "usuario")]
        public ActionResult EstadoCuentaImpresionConFechas()
        {
            string id = null;
            DateTime fechaInicio = DateTime.Now.AddDays(-30);
            DateTime fechaFin = DateTime.Now;

            if (HttpContext.Session["ID"] != null)
            {
                id = HttpContext.Session["ID"].ToString();
                string[] lista = id.Split('?');
                fechaInicio = DateTime.Parse(lista[0]);
                fechaFin = DateTime.Parse(lista[1]).AddDays(1);
                ViewBag.FechaFin = fechaFin.AddDays(-1).ToShortDateString();
            }
            else
            {
                ViewBag.FechaFin = fechaFin.ToShortDateString();
            }

            ViewBag.FechaInicio = fechaInicio.ToShortDateString();


            List<Documentos> doc = ddm.ConsultarEstadoCuentaConFecha(User.Identity.Name, fechaInicio, fechaFin);
            if (doc.Count > 0)
            {
                MembershipUser user = Membership.GetUser(User.Identity.Name);
                doc[0].rtn = user.Comment;
            }
            return PartialView(doc);
        }

        [Authorize(Roles = "usuario")]
        public ActionResult ImprimirEstadoCuentaConFechas()
        {
            var report = new Rotativa.MVC.ActionAsPdf("EstadoCuentaImpresionConFechas");
            //{
            //    FileName = "Estado de cuenta -Portal de proveedores MQA.pdf"
            //};

            //return new Rotativa.AspNetCore.ViewAsPdf("DemoPageSizePDF")
            //{
            //    PageSize = Rotativa.AspNetCore.Options.Size.A6,
            //};

            return report;

        }


        #endregion

    }
}