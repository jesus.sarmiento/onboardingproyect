﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal_Proveedor.Services.Utility
{
    public class LoggerManager : ILoggerManager
    {
        private static LoggerManager instance;
        private static Logger logger;
        private const string portalProveedorRules = "PortalProveedorRules";

        private LoggerManager()
        {

        }

        public static LoggerManager GetInatance()
        {
            if (instance == null)
                instance = new LoggerManager();
            return instance;
        }

        private Logger GetLogger(string theLogger)
        {
            if (LoggerManager.logger == null)
                LoggerManager.logger = LogManager.GetLogger(theLogger);
            return LoggerManager.logger;
        }

        public void Debug(string message, string arg = null)
        {
            if (arg == null)
                GetLogger(portalProveedorRules).Debug(message);
            else
                GetLogger(portalProveedorRules).Debug(message, arg);
        }

        public void Error(string message, string arg = null)
        {
            if (arg == null)
                GetLogger(portalProveedorRules).Error(message);
            else
                GetLogger(portalProveedorRules).Error(message, arg);
        }

        public void Info(string message, string arg = null)
        {
            if (arg == null)
                GetLogger(portalProveedorRules).Info(message);
            else
                GetLogger(portalProveedorRules).Info(message, arg);
        }

        public void Warning(string message, string arg = null)
        {
            if (arg == null)
                GetLogger(portalProveedorRules).Warn(message);
            else
                GetLogger(portalProveedorRules).Warn(message, arg);
        }
    }
}