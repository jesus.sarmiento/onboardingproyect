﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal_Proveedor.Models.Registro
{
    public class UsuariosIngresados
    {

        public int id { get; set; }
        public string usuario { get; set; }
        public DateTime fecha { get; set; }
    }
}