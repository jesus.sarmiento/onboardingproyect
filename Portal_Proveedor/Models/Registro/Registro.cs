﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Portal_Proveedor.Models.Registro
{
    public class Registro
    {
        [Required]
        public string UserName { get; set; }

        //[Required]
        //public string nombre { get; set; }

        //[Required]
        //public string apellidos { get; set; }

        [Required]
        [Email(ErrorMessage = "Debe ingresar una dirección de correo electrónico válida")]
        public string correo { get; set; }

        [Required]
        public string clave { get; set; }

        [Required]
        public string confirmaClave { get; set; }

        [Required]
        public bool esExtranjero { get; set; }

        public string extranjero { get; set; }

       // [Required]
        public string RTN { get; set; }

        //[Required]
        public string nombreEmpresa { get; set; }
        public string pais { get; set; }
        public string correoSAP { get; set; }
        public bool activo { get; set; }
        public string msjError { get; set; }
        public int codeError { get; set; }
    }
}