﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal_Proveedor.Models.Registro
{
    public class CambioClave
    {
        public string usuario           {get;set;}
        public string claveVieja        {get;set;}
        public string claveNueva        {get;set;}
        public string confirmacionClave {get;set;}
    }
}