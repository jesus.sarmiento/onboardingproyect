﻿using Portal_Proveedor.Models.Correo;
using Portal_Proveedor.Services.Utility;
using System;
using System.Collections.Generic;

namespace Portal_Proveedor.Models.Registro
{

    public class RegistroDM
    {
        private Datos.DatosDataContext bd = new Datos.DatosDataContext();
        
        public bool ExisteRTN(string rtn)
        {
            bool res = false;
            
            try
            {
                var obj = bd.ConsultarRTN(rtn);
                int cantidad = 0;
                foreach (var o in obj)
                {
                    cantidad = o.Cantidad.Value;
                }
                if (cantidad > 0)
                {
                    res = true;
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ExisteRTN in RegistoDM, System message: {e}");
            }
            
            return res;
        }

        public int RegistrarUsuario(Registro model)
        {
            int resultado = 0;
            
            try
            {
                string extranjero = "NO";

                if (model.esExtranjero)
                    extranjero = "SI";

                var o = bd.AgregarRegistroUsuario(model.UserName, model.pais, model.RTN, model.nombreEmpresa, model.correoSAP, model.esExtranjero);
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method RegistrarUsuario in RegistoDM, System message: {e}");
            }
            
            return resultado;
        }

        public Registro ValidarProveedor(Registro model)
        {
            try
            {
                Servicio.ZWS_TODO_900_DESClient cliente = new Servicio.ZWS_TODO_900_DESClient();
                Servicio.ZwsConsultaProveedores request = new Servicio.ZwsConsultaProveedores();
                Servicio.ZwsConsultaProveedoresResponse response = new Servicio.ZwsConsultaProveedoresResponse();

                cliente.ClientCredentials.UserName.UserName = Properties.Settings.Default.us_ws;
                cliente.ClientCredentials.UserName.Password = Properties.Settings.Default.pass_ws;

                request.Lifnr = model.UserName;
                response = cliente.ZwsConsultaProveedores(request);

                if (response.Data.Name1 != null && response.Data.Name1 != "")
                {
                    model.nombreEmpresa = response.Data.Name1;
                    model.correoSAP = response.Data.SmtpAddr;
                    model.pais = response.Data.Land1;
                    model.RTN = response.Data.Stcd1;
                    if (!response.Data.Bloqueado.Equals("X"))
                    {
                        model.activo = true;
                    }
                    else
                    {
                        model.activo = false;
                        model.codeError = -1;
                    }

                    int codigo = int.Parse(model.UserName);
                    string p = codigo.ToString().Substring(0, 1);

                    if (p.Equals("7"))
                    {
                        model.esExtranjero = true;
                    }

                }
                else
                {
                    model.activo = false;
                    model.codeError = -2;
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ValidarProveedor in RegistoDM, System message: {e}");
            }
            
            return model;
        }

        public int AgregarInicioSesion(string usuario)
        {
            int i = 0;

            try
            {
                var o = bd.AgregarInicioSesion(usuario, DateTime.Now);
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method AgregarInicioSesion in RegistoDM, System message: {e}");
            }

            return i;
        }

        public List<UsuariosIngresados> ListaDeUsuarios()
        {
            List<UsuariosIngresados> lista = new List<UsuariosIngresados>();
            
            try
            {
                int i = 1;
                var o = bd.ConsultarIniciosSesion();

                foreach (var obj in o)
                {
                    UsuariosIngresados usuario = new UsuariosIngresados();
                    usuario.id = i;
                    usuario.usuario = obj.UserName;
                    usuario.fecha = obj.FechaConexion;
                    lista.Add(usuario);
                    i++;
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ListaDeUsuarios in RegistoDM, System message: {e}");
            }
           
            return lista;
        }

        public string RegenerarClave(Registro model, string temporal)
        {
            int i = 0;
            string link = string.Empty;

            try
            {

                string id = EncriptarUsuario(model.UserName, model.correo, temporal);
                link = Properties.Settings.Default.RecuperarClave + id;

                var o = bd.CrearClaveTemporal(model.UserName, temporal);

                // Enviar el correo al cliente
                CorreoElectronicoDM cedm = new CorreoElectronicoDM();

                cedm.EnviarCorreo(model.correo, link, 2);
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method RegenerarClave in RegistoDM, System message: {e}");
            }
            
            return link;
        }

        public int ValidarClaveTempNoVencida(string usuario)
        {
            int id = 0;
            
            try
            {
                var o = bd.ValidarSiTieneCambioClaveActivo(usuario);

                foreach (var obj in o)
                {
                    id = obj.Id;
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ValidarClaveTempNoVencida in RegistoDM, System message: {e}");
            }
            
            return id;
        }

        public int MarcarClaveUsada(int id)
        {
            int i = 0;
            
            try
            {
                bd.MarcarClaveTempComoUsada(id);
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method MarcarClaveUsada in RegistoDM, System message: {e}");
            }

            return i;
        }

        private string EncriptarUsuario(string usuario, string correo, string temp)
        {
            string usEnc = string.Empty;
            
            try
            {
                string cadena = temp + "|" + usuario + "|" + correo;

                byte[] encryted = System.Text.Encoding.Unicode.GetBytes(cadena);
                usEnc = Convert.ToBase64String(encryted);
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method EncriptarUsuario in RegistoDM, System message: {e}");
            }

            return usEnc;
        }

        private CambioClave DesencriptarUsuario(string usEnc)
        {
            CambioClave model = new CambioClave();
            
            try
            {

                byte[] decryted = Convert.FromBase64String(usEnc);
                string result = System.Text.Encoding.Unicode.GetString(decryted);

                string[] division = result.Split('|');

                model.usuario = division[1];
                model.claveVieja = division[0];

            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method DesencriptarUsuario in RegistoDM, System message: {e}");
            }
            
            return model;
        }

        private string GenerarClaveTemporal()
        {
            string temporal = string.Empty;

            temporal = System.Web.Security.Membership.GeneratePassword(8, 2);

            return temporal;
        }

        public CambioClave ObtenerInfoCambioClave(string id)
        {
            CambioClave model = new CambioClave();

            model = DesencriptarUsuario(id);

            return model;
        }

        private void ProveedorExiste(string codigoProveedor)
        {
            Servicio.ZWS_TODO_900_DESClient cliente = new Servicio.ZWS_TODO_900_DESClient();
            Servicio.ZwsConsultaProveedores request = new Servicio.ZwsConsultaProveedores();
            Servicio.ZwsConsultaProveedoresResponse response = new Servicio.ZwsConsultaProveedoresResponse();

            cliente.ClientCredentials.UserName.UserName = "ws_onbase";
            cliente.ClientCredentials.UserName.Password = "0n34sezo!9%";

            request.Lifnr = codigoProveedor;
            response = cliente.ZwsConsultaProveedores(request);





            //consulta.Land1 -- clave del país
            //consulta.Name1 -- NOmbre 1
            //consulta.Name2 -- Nombre 2
            //consulta.SmtpAddr -- Correo electrónico
            //consulta.Stcd1 -- Número de identificación fiscal
        }

        public bool ProveedorBloqueado(Registro model)
        {
            bool resultado = false;
            
            try
            {

                Servicio.ZWS_TODO_900_DESClient cliente = new Servicio.ZWS_TODO_900_DESClient();
                Servicio.ZwsConsultaProveedores request = new Servicio.ZwsConsultaProveedores();
                Servicio.ZwsConsultaProveedoresResponse response = new Servicio.ZwsConsultaProveedoresResponse();

                cliente.ClientCredentials.UserName.UserName = Properties.Settings.Default.us_ws;
                cliente.ClientCredentials.UserName.Password = Properties.Settings.Default.pass_ws;

                request.Lifnr = model.UserName;
                response = cliente.ZwsConsultaProveedores(request);

                string bloqueado = response.Data.Bloqueado;

                if (bloqueado.Equals("X"))
                {
                    resultado = true;
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ProveedorBloqueado in RegistoDM, System message: {e}");
            }

            return resultado;
        }
    }
}