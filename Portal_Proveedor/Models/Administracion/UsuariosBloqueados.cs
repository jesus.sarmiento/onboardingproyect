﻿using System;

namespace Portal_Proveedor.Models.Administracion
{
    public class UsuariosBloqueados
    {
        public int consecutivo { get; set; }
        public string Usuario { get; set; }
        public string Email { get; set; }
        public bool Bloqueado { get; set; }
        public DateTime UltimaFechaBloqueo { get; set; }
    }
}