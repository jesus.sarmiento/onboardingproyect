﻿using System;

namespace Portal_Proveedor.Models.Administracion
{
    public class IntentosFallidos
    {
        public int consecutivo { get; set; }
        public string usuario { get; set; }
        public string Email { get; set; }
        public DateTime UltimaFechaLogueo { get; set; }
        public DateTime UltimaFechaCambioClave { get; set; }
        public int CantidadIntentosFallidos { get; set; }
        public DateTime FechaIntentosFallidos { get; set; }
    }
}