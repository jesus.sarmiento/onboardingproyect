﻿using Portal_Proveedor.Services.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal_Proveedor.Models.Administracion
{
    public class AdministracionDM
    {
        private Datos.DatosDataContext bd = new Datos.DatosDataContext();

        public Registro.Registro ConsultarDatosDelProveedor(string UserName)
        {
            Registro.Registro proveedor = new Registro.Registro();

            try
            {

                var obj = bd.ConsultarDatosCompletosPorCodigoProveedor(UserName);

                foreach (var o in obj)
                {
                    proveedor.correo = o.Email;
                    if ((bool)o.EsExtranjero)
                        proveedor.extranjero = "SI";
                    else
                        proveedor.extranjero = "NO";
                    proveedor.nombreEmpresa = o.Nombre;
                    proveedor.RTN = o.RTN;
                    proveedor.UserName = o.UserName;
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ConsultarDatosDelProveedor, System message: {e}");
            }

            return proveedor;
        }

        public List<IntentosFallidos> ReporteIntentosFallidos()
        {
            List<IntentosFallidos> lista = new List<IntentosFallidos>();
            
            try
            {
                int i = 1;

                var obj = bd.ConsultarUsuariosIntentosFallidos();

                foreach (var o in obj)
                {
                    IntentosFallidos intento = new IntentosFallidos();

                    intento.consecutivo = i;
                    intento.CantidadIntentosFallidos = o.IntentosFallidos;
                    intento.Email = o.Email;
                    intento.FechaIntentosFallidos = o.FechaIntentosFallidos;
                    intento.UltimaFechaCambioClave = o.UltimaFechaCambioPassword;
                    intento.UltimaFechaLogueo = o.UltimaFechaLogueo;
                    intento.usuario = o.UserName;

                    lista.Add(intento);

                    i++;
                }

            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ReporteIntentosFallidos, System message: {e}");
            }

            return lista;
            
        }

        public List<UsuariosBloqueados> ReporteUsuariosBloqueados()
        {
            List<UsuariosBloqueados> lista = new List<UsuariosBloqueados>();
            
            try
            {
                int i = 1;

                var obj = bd.ConsultarUsuariosBloqueados();

                foreach(var o in obj)
                {
                    UsuariosBloqueados b = new UsuariosBloqueados();

                    b.consecutivo = i;
                    b.Bloqueado = o.Bloqueado;
                    b.Email = o.Email;
                    b.UltimaFechaBloqueo = o.UltimaFechaBloqueo;
                    b.Usuario = o.UserName;

                    lista.Add(b);

                    i++;
                }

            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ReporteUsuariosBloqueados, System message: {e}");
            }
            
            return lista;
        }
    }
}