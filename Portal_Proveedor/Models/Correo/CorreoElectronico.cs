﻿namespace Portal_Proveedor.Models.Correo
{
    public class CorreoElectronico
    {

        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string IsBodyHtml { get; set; }
        public string Priority { get; set; }

    }
}