﻿using Portal_Proveedor.Models.OnBase;
using Portal_Proveedor.Services.Utility;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace Portal_Proveedor.Models.Correo
{
    public class CorreoElectronicoDM
    {


        private SmtpClient DefinirSmtp()
        {

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.office365.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("proveedores@cenosa.hn", "0n8a$33wa1l");

            return smtp;
        }

        private MailMessage DefinirMensajeCorreo()
        {
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress("franchesca.rodriguez@mqaamericas.com"));
            email.From = new MailAddress("proveedores@cenosa.hn");
            email.Subject = "Asunto ( " + DateTime.Now.ToString("dd / MM / yyyy hh:mm:ss") + " ) ";
            email.Body = "Cualquier contenido en <b>HTML</b> para enviarlo por correo electrónico.";
            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;
            return email;
        }

        private MailMessage DefinirMensajeCorreoParaOlvidoClave(string correoTo, string link)
        {
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(correoTo));
            email.From = new MailAddress("proveedores@cenosa.hn");
            email.Subject = "Reestablecer clave Portal de Proveedores CENOSA";
            email.Body = "Estimado usuario: </br>" +
                         "Para reestablecer su clave por favor ingrese al siguiente enlace: </br> " +
                         "<a href='" + link + "' class='btn btn-success'>Cambiar clave</a>";
            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;
            return email;
        }

        private MailMessage DefinirMensajeCorreoParaRegistroEnPortal(string correoTo)
        {
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(correoTo));
            email.From = new MailAddress("proveedores@cenosa.hn");
            email.Subject = "Registro en el Portal de Proveedores CENOSA";
            email.Body = "Estimado usuario: </br>" +
                         "Se ha realizado su registro exitosamente, en el portal de proveedores de CENOSA.";
            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;
            return email;
        }

        private MailMessage DefinirMensajeCorreoParaRegistroDeFactura(string correoTo, string numFactura)
        {
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(correoTo));
            email.From = new MailAddress("proveedores@cenosa.hn");
            email.Subject = "Registro de facturas";
            email.Body = "Estimado usuario: </br>" +
                         "Se ha realizado el registro de la factura " + numFactura + " exitosamente, en el portal de proveedores de CENOSA.";
            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;
            return email;
        }

        private MailMessage DefinirMensajeCorreoParaRegistroDeFacturaMasiva(string correoTo, List<ExcelResultado> numFactura)
        {
            string totalDeFacturas = "";
            foreach (ExcelResultado i in numFactura)
            {
                totalDeFacturas = totalDeFacturas + " <br /> " + i.numeroFiscal + " - " + i.Estado;
            }

            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(correoTo));
            email.From = new MailAddress("proveedores@cenosa.hn");
            email.Subject = "Registro de facturas";
            email.Body = "Estimado usuario: </br>" +
                         "Se ha realizado el registro  de las siguientes facturas " + totalDeFacturas + ", en el portal de proveedores de CENOSA.";
            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;
            return email;
        }

        private MailMessage DefinirMensajeCorreoParaRegistroDeConstanciaPagoCuenta(string correoTo, string fechaVencimiento)
        {
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(correoTo));
            email.From = new MailAddress("proveedores@cenosa.hn");
            email.Subject = "Registro Constancia de Pago a Cuenta";
            email.Body = "Estimado usuario: </br>" +
                         "Se ha realizado el registro de la Constancia de Pago a Cuenta, con fecha de vencimiento " + fechaVencimiento + " exitosamente.";
            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;
            return email;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="correoTo"></param>
        /// <param name="link"></param>
        /// <param name="tipo">
        /// 1 - Correo de registro
        /// 2 - Correo de recuerar clave
        /// 3 - Correo de constancia de pago a cuenta
        /// 4 - Correo de registro de factura
        /// 5 - Correo de registro de factura carga masiva
        /// </param>
        public void EnviarCorreo(string correoTo, string link, int tipo)
        {
            try
            {
                SmtpClient smtp = DefinirSmtp();
                MailMessage email = new MailMessage();

                switch (tipo)
                {
                    case 1:
                        email = DefinirMensajeCorreoParaRegistroEnPortal(correoTo);
                        break;
                    case 2:
                        email = DefinirMensajeCorreoParaOlvidoClave(correoTo, link);
                        break;
                    case 3:
                        email = DefinirMensajeCorreoParaRegistroDeConstanciaPagoCuenta(correoTo, link);
                        break;
                    case 4:
                        email = DefinirMensajeCorreoParaRegistroDeFactura(correoTo, link);
                        break;
                    default:
                        break;
                }
                smtp.Send(email);
                email.Dispose();
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method EnviarCorreo in CorreoElectronicoDM, System message: {e}");
                //  output = "Error enviando correo electrónico: " + ex.Message;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="correoTo"></param>
        /// <param name="link"></param>
        /// <param name="tipo">
        /// 5 - Correo de registro de factura carga masiva
        /// </param>
        public void EnviarCorreo(string correoTo, List<ExcelResultado> link, int tipo)
        {
            try
            {
                SmtpClient smtp = DefinirSmtp();
                MailMessage email = new MailMessage();

                switch (tipo)
                {
                    case 5:
                        email = DefinirMensajeCorreoParaRegistroDeFacturaMasiva(correoTo, link);
                        break;
                    default:
                        break;
                }
                smtp.Send(email);
                email.Dispose();
                // output = "Corre electrónico fue enviado satisfactoriamente.";
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method EnviarCorreo in CorreoElectronicoDM, System message: {e}");
            }

        }

    }
}