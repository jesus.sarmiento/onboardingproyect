﻿namespace Portal_Proveedor.Models.OnBase
{
    public class ExcelResultado
    {
        public int consecutivo { get; set; }
        public string codigoProveedor { get; set; }
        public string numeroPO { get; set; }
        public string numeroFiscal { get; set; }
        public string Estado { get; set; }

    }
}