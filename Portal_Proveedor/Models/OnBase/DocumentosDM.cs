﻿using Hyland.Unity;
using Hyland.Unity.Extensions;
using NLog;
using Portal_Proveedor.Services.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web;

namespace Portal_Proveedor.Models.OnBase
{
    public class DocumentosDM
    {
        private Datos.DatosDataContext bd = new Datos.DatosDataContext();
        private Datos.OnBaseDataContext ob = new Datos.OnBaseDataContext();

        public List<Documentos> ConsultaDocumentos(string codigoProveedor)
        {
            List<Documentos> lista = new List<Documentos>();
            Application app = null;
            app = Conexion.ConectarOnBase(app, codigoProveedor);
            DocumentList documents = null;
            int i = 1;

            DocumentType docType = app.Core.DocumentTypes.Find(363);
            if (docType == null)
            {
                throw new Exception("Documento no encontrado");
            }

            DocumentQuery document = app.Core.CreateDocumentQuery();
            document.AddDocumentType(docType);

            if (codigoProveedor != "admin")
            {
                document.AddKeyword("Código proveedor", codigoProveedor, KeywordOperator.Equal, KeywordRelation.Or);
            }
            //document.AddKeyword("Estado", "SIN FACTURA", KeywordOperator.Equal, KeywordRelation.Or); //si el estado es nulo o rechazado
            //document.AddKeyword("Estado", "FACTURA RECHAZADA", KeywordOperator.Equal, KeywordRelation.Or);
            document.AddKeyword("Monto restante", 0, KeywordOperator.GreaterThan, KeywordRelation.Or);

            documents = document.Execute(100);

            foreach (Document doc in documents)
            {
                // doc.KeywordRecords.
                Document documento = app.Core.GetDocumentByID(doc.ID);

                if (documento != null)
                {
                    Documentos d = new Documentos();
                    d.idPO = doc.ID;
                    KeywordRecordList keyRecList = documento.KeywordRecords;

                    foreach (KeywordRecord rec in keyRecList)
                    {

                        foreach (Keyword key in rec.Keywords)
                        {
                            if (key.KeywordType.Name == "Código proveedor")
                            {
                                d.codigoProveedor = key.AlphaNumericValue;
                            }
                            if (key.KeywordType.Name == "Número de PO")
                            {
                                d.numPO = key.AlphaNumericValue;
                            }
                            if (key.KeywordType.Name == "Monto total")
                            {
                                d.montoPO = key.CurrencyValue.ToString();
                            }
                            if (key.KeywordType.Name == "Monto restante")
                            {
                                d.montoRestante = key.CurrencyValue.ToString();
                            }
                        }
                        d.consecutivo = i;
                        //var obj = bd.ConsultarNumMigo(d.numPO, d.codigoProveedor);
                        //foreach (var o in obj)
                        //{
                        //    d.numMigo = o.NumeroMigo;
                        //}
                        i++;
                    }

                    DefaultDataProvider defaultDataProvider = app.Core.Retrieval.Default;
                    // Use a using statement to get the PageData (for cleanup), and then save the document to disk. To Save, use the Save method above.
                    using (PageData pageData = defaultDataProvider.GetDocument(documento.DefaultRenditionOfLatestRevision))
                    {
                        Stream stream = pageData.Stream;

                        byte[] result;
                        using (var streamReader = new MemoryStream())
                        {
                            stream.CopyTo(streamReader);
                            result = streamReader.ToArray();
                        }

                        string docOB = Convert.ToBase64String(result);

                        d.documentoPO = "data:application/pdf;base64," + docOB;
                    }

                    lista.Add(d);
                }
            }

            app = Conexion.DesconectarOnBase(app);

            return lista;
        }

        public List<Documentos> ConsultaOrdenesCompra(string codigoProveedor)
        {
            LoggerManager.GetInatance().Info($"Test logger");

            List<Documentos> lista = new List<Documentos>();

            // Consulta las ordenes de compra - en SAP
            lista = ConsultarOrdenesCompraProveedor(codigoProveedor);

            // Consultar el doc OC - en OnBase
            lista = ConsultarDocumentosPDFOrdenCompra(codigoProveedor, lista);

            List<Documentos> facturasOB = ConsultarFacturasEnOnBasePorProveedoryEstado(codigoProveedor);

            lista = ValidarOCyFacturasOB(lista, facturasOB);

            return lista;
        }

        private List<Documentos> ValidarOCyFacturasOB(List<Documentos> OC, List<Documentos> Facturas)
        {
            List<Documentos> remover = new List<Documentos>();

            try
            {
                foreach (Documentos o in OC)
                {
                    foreach (Documentos d in Facturas)
                    {
                        if (d.numPO == o.numPO)
                        {
                            decimal montofactura = 0;
                            if (d.montoTotal != null)
                                montofactura = decimal.Parse(d.montoTotal);
                            decimal montoNuevo = decimal.Parse(o.montoRestante) - montofactura;
                            o.montoRestante = montoNuevo.ToString();
                        }
                    }
                    decimal monto = decimal.Parse(o.montoRestante);
                    if (monto <= 1)
                    {
                        remover.Add(o);
                    }
                }

                foreach (Documentos r in remover)
                {
                    OC.Remove(r);
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ValidarOCyFacturasOB in DocumentosDM, System message: {e}");
            }
            
            return OC;
        }

        private List<Documentos> ConsultarOrdenesCompraProveedor(string codigoProveedor)
        {
            List<Documentos> lista = new List<Documentos>();
            
            try
            {
                Servicio.ZWS_TODO_900_DESClient cliente = new Servicio.ZWS_TODO_900_DESClient();
                Servicio.ZwsConsultaOrdenesRequest request = new Servicio.ZwsConsultaOrdenesRequest();
                Servicio.ZwsConsultaOrdenesResponse response = new Servicio.ZwsConsultaOrdenesResponse();

                cliente.ClientCredentials.UserName.UserName = Properties.Settings.Default.us_ws;
                cliente.ClientCredentials.UserName.Password = Properties.Settings.Default.pass_ws;

                request.ZwsConsultaOrdenes = new Servicio.ZwsConsultaOrdenes();
                request.ZwsConsultaOrdenes.Lifnr = codigoProveedor;
                response = cliente.ZwsConsultaOrdenes(request.ZwsConsultaOrdenes);

                Servicio.ZtytSCabOrdenes[] cab = response.DataCab;
                Servicio.ZtytSPosOrdenes[] pos = response.DataPos;

                int consecutivo = 1;

                foreach (Servicio.ZtytSCabOrdenes i in cab)
                {
                    Documentos doc = new Documentos();
                    doc.consecutivo = consecutivo;
                    doc.numPO = i.Ebeln;
                    consecutivo++;
                    lista.Add(doc);
                }

                foreach (Servicio.ZtytSPosOrdenes j in pos)
                {
                    foreach (Documentos d in lista)
                    {
                        if (j.Ebeln == d.numPO)
                        {
                            if (j.Mwskz == "V1" || j.Mwskz == "V2" || j.Mwskz == "V3")
                            {
                                double montoSI = double.Parse(j.Netwr.ToString());
                                double impuesto = 0.15;
                                double valor = montoSI * impuesto;
                                double totalFinal = montoSI + valor;
                                if (d.montoRestante == null)
                                    d.montoRestante = "0";
                                double montoAcumulado = double.Parse(d.montoRestante);
                                double montoTotal = montoAcumulado + totalFinal;
                                d.montoRestante = montoTotal.ToString();
                                // acumular el monto
                            }
                            else
                            {
                                // acumular el monto
                                if (d.montoRestante == null || d.montoRestante == "")
                                    d.montoRestante = "0";
                                decimal montoAcumulado = decimal.Parse(d.montoRestante);
                                decimal montoTotal = montoAcumulado + j.Netwr;
                                d.montoRestante = montoTotal.ToString();
                            }
                            d.numMigo = d.numMigo + j.Belnr + "l";
                        }
                    }
                }

            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ConsultarOrdenesCompraProveedor in DocumentosDM, System message: {e}");
            }
            
            return lista;
        }

        private List<Documentos> ConsultarFacturasEnOnBase(string codigoProveedor)
        {
            List<Documentos> lista = new List<Documentos>();
            
            try
            {
                int consecutivo = 1;

                //CustomQuery consulta = app.Core.CustomQueries.Find(104);
                var obj = ob.ConsultarFacturasPorProveedoryFactura(codigoProveedor, "");

                foreach (var o in obj)
                {
                    Documentos d = new Documentos();

                    //Document documento = app.Core.GetDocumentByID(resultItem.Document.ID);
                    d.consecutivo = consecutivo;
                    d.codigoProveedor = o.CodigoProveedor;
                    d.numPO = o.OC;
                    if (o.MontoTotal != null)
                        d.montoTotal = o.MontoTotal.Value.ToString();
                    d.numFiscal = o.NumFactura;
                    d.estado = o.Estado;

                    lista.Add(d);
                    consecutivo++;
                }

                // }

                //app = Conexion.DesconectarOnBase(app);
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ConsultarFacturasEnOnBase in DocumentosDM, System message: {e}");
            }
            
            return lista;
        }

        private List<Documentos> ConsultarFacturasEnOnBasePorProveedoryEstado(string codigoProveedor)
        {
            List<Documentos> lista = new List<Documentos>();
            
            try
            {
                int consecutivo = 1;
                //CustomQuery consulta = app.Core.CustomQueries.Find(107);
                var obj = ob.ConsultarFacturaPorProveedoryEstado(codigoProveedor);

                foreach (var o in obj)
                {
                    Documentos d = new Documentos();

                    //Document documento = app.Core.GetDocumentByID(resultItem.Document.ID);
                    d.consecutivo = consecutivo;
                    d.codigoProveedor = o.CodigoProveedor;
                    d.numPO = o.OC;
                    if (o.MontoTotal != null)
                        d.montoTotal = o.MontoTotal.Value.ToString();
                    d.numFiscal = o.NumFactura;
                    d.estado = o.Estado;

                    lista.Add(d);
                    consecutivo++;
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ConsultarFacturasEnOnBasePorProveedoryEstado in DocumentosDM, System message: {e}");
            }
            
            return lista;
        }

        private Keyword CrearKeyword(Application app, string nomKw, string datosKw)
        {
            KeywordType kwType = app.Core.KeywordTypes.Find(nomKw);
            Keyword kw = null;
            if (!kwType.TryCreateKeyword(datosKw, out kw))
            {
                LoggerManager.GetInatance().Info($"No se ha podido crear el kw: {nomKw}");
                throw new Exception("No se ha podido crear el kw: " + nomKw);
            }

            return kw;
        }

        private Keyword CrearKeyword(Application app, string nomKw, decimal datosKw)
        {
            KeywordType kwType = app.Core.KeywordTypes.Find(nomKw);
            Keyword kw = null;
            if (!kwType.TryCreateKeyword(Math.Round(datosKw, 2), out kw))
            {
                LoggerManager.GetInatance().Info($"No se ha podido crear el kw: {nomKw}");
                throw new Exception("No se ha podido crear el kw: " + nomKw);
            }

            return kw;
        }

        private Keyword CrearKeyword(Application app, string nomKw, float datosKw)
        {
            KeywordType kwType = app.Core.KeywordTypes.Find(nomKw);
            Keyword kw = null;
            if (!kwType.TryCreateKeyword(Math.Round(datosKw, 2), out kw))
            {
                LoggerManager.GetInatance().Info($"No se ha podido crear el kw: {nomKw}");
                throw new Exception("No se ha podido crear el kw: " + nomKw);
            }

            return kw;
        }

        private Keyword CrearKeyword(Application app, string nomKw, DateTime datosKw)
        {
            KeywordType kwType = app.Core.KeywordTypes.Find(nomKw);
            Keyword kw = null;
            if (!kwType.TryCreateKeyword(datosKw, out kw))
            {
                LoggerManager.GetInatance().Info($"No se ha podido crear el kw: {nomKw}");
                throw new Exception("No se ha podido crear el kw: " + nomKw);
            }

            return kw;
        }

        public long ImportarDocumentoPath(Documentos model)
        {
            Application app = null;
            long newDocID = -1;

            try
            {
                app = Conexion.ConectarOnBase(app, model.codigoProveedor);

                //decimal monto = decimal.Parse((model.montoTotal.Replace('.', ',')));
                decimal monto = decimal.Parse((model.montoTotal));

                decimal montoPO = decimal.Parse(model.montoRestante) + decimal.Parse(Properties.Settings.Default.Tolerancia);

                bool existeFactura = false;

                List<Documentos> facturasOB = ConsultarFacturasEnOnBase(model.codigoProveedor);

                foreach (Documentos l in facturasOB)
                {
                    if (l.numFiscal == model.numFiscal)
                    {
                        if (l.estado == "EN ESPERA DE VALIDACION DE LA FACTURA" || l.estado == "EN ESPERA DE VERIFICACION DE FACTURA FISICA")
                        {
                            existeFactura = true;
                        }
                    }
                }

                // Consultar que no exista ese número de factura
                // Si esta rechazado si puede ingresar el mismo número
                if (!existeFactura)
                {
                    // Valida que el monto ingresado no sea mayor al monto de la OC
                    if (montoPO >= monto)
                    {
                        Stream documentConverted = model.filePathFactura.InputStream;

                        using (PageData pageData = app.Core.Storage.CreatePageData(documentConverted, "PDF"))
                        {
                            DocumentType docType = app.Core.DocumentTypes.Find(105); // Documento tipo factura
                            FileType imageFileType = app.Core.FileTypes.Find(16);
                            StoreNewDocumentProperties props = app.Core.Storage.CreateStoreNewDocumentProperties(docType, imageFileType);

                            props.AddKeyword(CrearKeyword(app, "Código del proveedor", model.codigoProveedor));
                            props.AddKeyword(CrearKeyword(app, "Número de Orden de Compra", model.numPO));
                            props.AddKeyword(CrearKeyword(app, "Número de factura", model.numFiscal));
                            props.AddKeyword(CrearKeyword(app, "Monto sin Impuestos", float.Parse(model.montoSinImpuesto)));
                            props.AddKeyword(CrearKeyword(app, "Impuestos", float.Parse(model.impuesto)));
                            props.AddKeyword(CrearKeyword(app, "Monto total", float.Parse(model.montoTotal)));
                            props.AddKeyword(CrearKeyword(app, "Número de entrada de mercancía / servicio", model.numMigo));
                            props.AddKeyword(CrearKeyword(app, "Número CAI", model.numeroCAI));
                            props.AddKeyword(CrearKeyword(app, "Fecha Factura", DateTime.Parse(model.fechaDeEmision)));
                            //props.AddKeyword(CrearKeyword(app, "Fecha de Vencimiento de la Factura", DateTime.Parse(model.fechaDeVencimiento)));


                            Document newDoc = app.Core.Storage.StoreNewDocument(pageData, props);

                            newDocID = newDoc.ID;

                            if (newDocID > 0)
                            {
                                ImportarDocumentoSoportePath(app, model);
                            }
                        }
                    }
                    else
                    {
                        newDocID = -3; // el monto de la PO es menor al que se desea ingresar
                    }
                }
                else
                {
                    newDocID = -2; // el número de factura ya existe
                }
                app = Conexion.DesconectarOnBase(app);
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ImportarDocumentoPath in DocumentosDM, System message: {e}");
            }
            
            return newDocID;
        }

        public long ImportarDocumento(Documentos model)
        {
            Application app = null;
            long newDocID = -1;

            try
            {
                app = Conexion.ConectarOnBase(app, model.codigoProveedor);
                Stream documentConverted = model.filePathFactura.InputStream;

                using (PageData pageData = app.Core.Storage.CreatePageData(documentConverted, "PDF"))
                {
                    DocumentType docType = app.Core.DocumentTypes.Find(105); // Documento tipo factura
                    FileType imageFileType = app.Core.FileTypes.Find(16);
                    StoreNewDocumentProperties props = app.Core.Storage.CreateStoreNewDocumentProperties(docType, imageFileType);

                    props.AddKeyword(CrearKeyword(app, "Código del proveedor", model.codigoProveedor));
                    props.AddKeyword(CrearKeyword(app, "Número de Orden de Compra", model.numPO));
                    props.AddKeyword(CrearKeyword(app, "Número de factura", model.numFiscal));
                    props.AddKeyword(CrearKeyword(app, "Monto sin Impuestos", decimal.Parse(model.montoSinImpuesto)));
                    props.AddKeyword(CrearKeyword(app, "Impuestos", model.impuesto));
                    props.AddKeyword(CrearKeyword(app, "Monto total", decimal.Parse(model.montoTotal)));
                    props.AddKeyword(CrearKeyword(app, "Número de entrada de mercancía / servicio", model.numMigo));
                    props.AddKeyword(CrearKeyword(app, "Número CAI", model.numeroCAI));
                    props.AddKeyword(CrearKeyword(app, "Fecha Factura", DateTime.Parse(model.fechaDeEmision)));
                    props.AddKeyword(CrearKeyword(app, "Fecha de Vencimiento de la Factura", DateTime.Parse(model.fechaDeVencimiento)));


                    Document newDoc = app.Core.Storage.StoreNewDocument(pageData, props);

                    newDocID = newDoc.ID;

                    if (newDocID > 0)
                    {
                        ImportarDocumentoSoportePath(app, model);
                    }
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ImportarDocumento in DocumentosDM, System message: {e}");
            }
            
            return newDocID;
        }

        public long ImportarDocumentoSoportePath(Application app, Documentos model)
        {
            long newDocID = -1;
            
            try
            {
                if (model.files != null)
                {
                    foreach (var i in model.files)
                    {
                        if (i != null)
                        {
                            Stream documentConverted = i.InputStream;

                            using (PageData pageData = app.Core.Storage.CreatePageData(documentConverted, "PDF"))
                            {
                                DocumentType docType = app.Core.DocumentTypes.Find(128); // Documento soporte                    
                                FileType imageFileType = app.Core.FileTypes.Find(16);
                                StoreNewDocumentProperties props = app.Core.Storage.CreateStoreNewDocumentProperties(docType, imageFileType);

                                props.AddKeyword(CrearKeyword(app, "Código del proveedor", model.codigoProveedor));
                                props.AddKeyword(CrearKeyword(app, "Número de Orden de Compra", model.numPO));
                                props.AddKeyword(CrearKeyword(app, "Número de factura", model.numFiscal));


                                Document newDoc = app.Core.Storage.StoreNewDocument(pageData, props);

                                newDocID = newDoc.ID;
                            }
                        }
                    }
                }
                else if (model.docSoporte != null)
                {
                    Stream documentConverted = model.docSoporte.InputStream;

                    using (PageData pageData = app.Core.Storage.CreatePageData(documentConverted, "PDF"))
                    {
                        DocumentType docType = app.Core.DocumentTypes.Find(128); // Documento soporte                    
                        FileType imageFileType = app.Core.FileTypes.Find(16);
                        StoreNewDocumentProperties props = app.Core.Storage.CreateStoreNewDocumentProperties(docType, imageFileType);

                        props.AddKeyword(CrearKeyword(app, "Código del proveedor", model.codigoProveedor));
                        props.AddKeyword(CrearKeyword(app, "Número de Orden de Compra", model.numPO));
                        props.AddKeyword(CrearKeyword(app, "Número de factura", model.numFiscal));


                        Document newDoc = app.Core.Storage.StoreNewDocument(pageData, props);

                        newDocID = newDoc.ID;
                    }
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ImportarDocumentoSoportePath in DocumentosDM, System message: {e}");
            }
            
            return newDocID;
        }

        public long ImportarDocumentoSoportePath(Application app, Documentos model, string filePath)
        {
            long newDocID = -1;

            using (PageData pageData = app.Core.Storage.CreatePageData(filePath))
            {
                DocumentType docType = app.Core.DocumentTypes.Find(128); // Documento soporte

                FileType imageFileType = app.Core.FileTypes.Find(16);

                StoreNewDocumentProperties props = app.Core.Storage.CreateStoreNewDocumentProperties(docType, imageFileType);

                props.AddKeyword(CrearKeyword(app, "Código del proveedor", model.codigoProveedor));
                props.AddKeyword(CrearKeyword(app, "Número de Orden de Compra", model.numPO));
                props.AddKeyword(CrearKeyword(app, "Número de factura", model.numFiscal));


                Document newDoc = app.Core.Storage.StoreNewDocument(pageData, props);

                newDocID = newDoc.ID;
            }

            //app = Conexion.DesconectarOnBase(app);
            return newDocID;
        }

        public long ImportarDocumentoPath(Application app, Documentos model, string filePath)
        {
            long newDocID = -1;

            using (PageData pageData = app.Core.Storage.CreatePageData(filePath))
            {
                DocumentType docType = app.Core.DocumentTypes.Find(105);

                FileType imageFileType = app.Core.FileTypes.Find(16);

                StoreNewDocumentProperties props = app.Core.Storage.CreateStoreNewDocumentProperties(docType, imageFileType);

                props.AddKeyword(CrearKeyword(app, "Código del proveedor", model.codigoProveedor));
                props.AddKeyword(CrearKeyword(app, "Número de Orden de Compra", model.numPO));
                props.AddKeyword(CrearKeyword(app, "Número de factura", model.numFiscal));
                props.AddKeyword(CrearKeyword(app, "Monto sin Impuestos", decimal.Parse(model.montoSinImpuesto)));
                props.AddKeyword(CrearKeyword(app, "Impuestos", model.impuesto));
                props.AddKeyword(CrearKeyword(app, "Monto total", decimal.Parse(model.montoTotal)));
                props.AddKeyword(CrearKeyword(app, "Número de entrada de mercancía / servicio", model.numMigo));

                props.AddKeyword(CrearKeyword(app, "Número CAI", model.numeroCAI));
                props.AddKeyword(CrearKeyword(app, "Fecha Factura", DateTime.Parse(model.fechaDeEmision)));
                props.AddKeyword(CrearKeyword(app, "Fecha de Vencimiento de la Factura", DateTime.Parse(model.fechaDeVencimiento)));

                Document newDoc = app.Core.Storage.StoreNewDocument(pageData, props);

                newDocID = newDoc.ID;
            }

            return newDocID;
        }

        private void CambiarPOconFactura(Application app, long DocumentID)
        {
            int i = 1;
            Document doc = app.Core.GetDocumentByID(DocumentID);
            if (doc == null)
            {
                throw new Exception("Documento no encontrado.");
            }

            KeywordType tieneFactura = app.Core.KeywordTypes.Find("Estado");
            if (tieneFactura == null)
            {
                throw new Exception("KW no encontrado.");
            }

            Keyword tieneFacturaKeyword = null;
            if (!tieneFactura.TryCreateKeyword("SI", out tieneFacturaKeyword))
            {
                throw new Exception("Keyword no encontrado.");
            }

            KeywordModifier kwMod = doc.CreateKeywordModifier();

            KeywordRecord kwRec = doc.KeywordRecords.Find(tieneFactura);

            if (kwRec != null)
            {
                Keyword oldStatus = kwRec.Keywords.Find(tieneFactura);
                kwMod.UpdateKeyword(oldStatus, tieneFacturaKeyword);
            }
            else
            {
                kwMod.AddKeyword(tieneFacturaKeyword);
            }

            using (DocumentLock docLock = doc.LockDocument()) // Se usa para que el doc se desbloquee al finalizar
            {
                if (docLock.Status == DocumentLockStatus.AlreadyLocked)
                {
                    throw new Exception("Documento esta bloqueado.");
                }

                kwMod.ApplyChanges();
            }

        }

        //public List<ExcelResultado> CargarMasivamenteDocumentos(HttpPostedFileBase file, string codProveedor)
        //{
        //    Application app = null;
        //    app = Conexion.ConectarOnBase(app, codProveedor);
        //    long i = 0;
        //    int c = 1;
        //    List<ExcelResultado> lista = new List<ExcelResultado>();

        //    CargaExcelDM edm = new CargaExcelDM();
        //    DataTable excel = edm.ObtenerDatos(file.InputStream, false);

        //    foreach (DataRow fila in excel.Rows)
        //    {
        //        ExcelResultado l = new ExcelResultado();
        //        Documentos d = new Documentos();

        //        d.codigoProveedor = fila.ItemArray[0].ToString();
        //        d.numPO = fila.ItemArray[1].ToString();
        //        d.numMigo = fila.ItemArray[2].ToString();
        //        d.numFiscal = fila.ItemArray[3].ToString();
        //        d.montoSinImpuesto = fila.ItemArray[4].ToString();
        //        d.impuesto = fila.ItemArray[5].ToString();
        //        d.montoTotal = fila.ItemArray[6].ToString();

        //        string ruta = fila.ItemArray[7].ToString();
        //        //if (d.montoSinImpuesto != "" &&
        //        //   d.impuesto != "" &&
        //        //   d.montoTotal != "")
        //        //{
        //        decimal montoSinImpuesto = long.Parse(d.montoSinImpuesto);
        //        decimal impuesto = long.Parse(d.impuesto);
        //        decimal monto = long.Parse(d.montoTotal);
        //        decimal monto2 = montoSinImpuesto + impuesto;

        //        //validar si existe el número fiscal
        //        //Validar si hay monto disponible en la PO
        //        if (monto.Equals(monto2))
        //        {
        //            if (codProveedor.Equals(d.codigoProveedor))
        //            {
        //                if (ValidarNumeroMigo(d.codigoProveedor, d.numPO, d.numMigo ))
        //                {
        //                    i = ImportarDocumentoPath(app, d, ruta);
        //                    if (i > 0)
        //                    {
        //                        l.Estado = "Factura cargada exitosamente.";
        //                    }
        //                    else
        //                    {
        //                        l.Estado = "No se cargo la factura.";
        //                    }
        //                }
        //                else
        //                {
        //                    l.Estado = "No se cargo la factura. Debe validar el número migo.";
        //                }
        //            }
        //            else
        //            {
        //                l.Estado = "El código de proveedor no coincide con el usuario.";
        //            }
        //        }
        //        else
        //        {
        //            l.Estado = "El monto total no coincide con la sumatoria del monto y los impuestos.";
        //        }

        //        l.codigoProveedor = d.codigoProveedor;
        //        l.numeroPO = d.numPO;
        //        l.numeroFiscal = d.numFiscal;
        //        l.consecutivo = c;
        //        c++;

        //        lista.Add(l);
        //    }
        //    app = Conexion.DesconectarOnBase(app);
        //    return lista;
        //}

        public List<ExcelResultado> CargarMasivamenteDocumentos(HttpPostedFileBase file, List<Documentos> listaPO, string codProveedor, IEnumerable<HttpPostedFileBase> files)
        {
            long i = 0;
            int c = 1;
            Application app = null;
            List<ExcelResultado> lista = new List<ExcelResultado>();

            CargaExcelDM edm = new CargaExcelDM();
            
            try
            {
                app = Conexion.ConectarOnBase(app, codProveedor);
                DataTable excel = edm.ObtenerDatos(file.InputStream, false);

                //foreach (HttpPostedFileBase f in files)
                //{
                //    string algo = f.FileName;


                foreach (DataRow fila in excel.Rows)
                {
                    HttpPostedFileBase factura = null;
                    HttpPostedFileBase soporte = null;
                    if (fila.ItemArray[10].ToString() != null)
                    {
                        foreach (HttpPostedFileBase f in files)
                        {
                            string algo = f.FileName;
                            if (fila.ItemArray[10].ToString().Equals(algo))
                            {
                                factura = f;
                            }
                            else if (fila.ItemArray[11].ToString().Equals(algo))
                            {
                                soporte = f;
                            }

                        }
                    }

                    if (fila.ItemArray[1].ToString() != "")
                    {
                        if (factura != null)
                        {
                            ExcelResultado l = new ExcelResultado();
                            Documentos d = new Documentos();

                            d.codigoProveedor = fila.ItemArray[0].ToString();
                            d.numPO = fila.ItemArray[1].ToString();
                            d.numMigo = fila.ItemArray[2].ToString();
                            d.numFiscal = fila.ItemArray[3].ToString();
                            d.numeroCAI = fila.ItemArray[4].ToString();
                            d.fechaDeEmision = fila.ItemArray[5].ToString();
                            d.fechaDeVencimiento = fila.ItemArray[6].ToString();
                            d.montoSinImpuesto = fila.ItemArray[7].ToString();
                            d.impuesto = fila.ItemArray[8].ToString();
                            d.montoTotal = fila.ItemArray[9].ToString();
                            d.filePathFactura = factura;
                            d.docSoporte = soporte;

                            if (d.montoSinImpuesto == null || d.montoSinImpuesto == "")
                                d.montoSinImpuesto = "0";
                            if (d.impuesto == null || d.impuesto == "")
                                d.impuesto = "0";
                            if (d.montoTotal == null || d.montoTotal == "")
                                d.montoTotal = "0";


                            decimal montoSinImpuesto = Math.Round(decimal.Parse(d.montoSinImpuesto, new CultureInfo("en-US")), 2);
                            decimal impuesto = Math.Round(decimal.Parse(d.impuesto, new CultureInfo("en-US")), 2);
                            decimal monto = Math.Round(decimal.Parse(d.montoTotal, new CultureInfo("en-US")), 2);
                            decimal monto2 = montoSinImpuesto + impuesto;

                            //validar si existe el número fiscal

                            bool existeFactura = false;

                            List<Documentos> facturasOB = ConsultarFacturasEnOnBase(codProveedor);

                            foreach (Documentos doc in facturasOB)
                            {
                                if (doc.numFiscal == d.numFiscal)
                                {
                                    if (doc.estado == "EN ESPERA DE VALIDACION DE LA FACTURA" || doc.estado == "EN ESPERA DE VERIFICACION DE FACTURA FISICA")
                                    {
                                        existeFactura = true;
                                        break;
                                    }
                                }
                            }

                            if (codProveedor.Equals(d.codigoProveedor))
                            {
                                if (!existeFactura)
                                {
                                    if (monto.Equals(monto2))
                                    {
                                        if (d.numFiscal != "")
                                        {
                                            //Existe PO
                                            foreach (var item in listaPO)
                                            {
                                                if (item.numPO.Equals(d.numPO))
                                                {
                                                    //Validar si hay monto disponible en la PO
                                                    decimal montoPO = decimal.Parse(item.montoRestante, new CultureInfo("en-US")) + decimal.Parse(Properties.Settings.Default.Tolerancia, new CultureInfo("en-US")); //Se agrega por tolerancia
                                                    if (item.montoRestante != null && montoPO >= monto)
                                                    {
                                                        //Validar que el numero MIGO o de servicio corresponda a la OC
                                                        if (ValidarNumeroMigo(d.codigoProveedor, d.numPO, d.numMigo, item.numMigo))
                                                        {
                                                            //i = ImportarDocumentoPath(app, d, ruta);
                                                            try
                                                            {
                                                                i = ImportarDocumento(d);
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                l.Estado = "No se cargo la factura. " + ex.Message;
                                                                i = -1;
                                                            }
                                                            if (i == -1)
                                                            { }
                                                            else if (i > 0)
                                                            {
                                                                l.Estado = "Factura cargada exitosamente.";
                                                            }
                                                            else
                                                            {
                                                                l.Estado = "No se cargo la factura.";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            l.Estado = "No se cargo la factura. Debe validar el número migo.";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        l.Estado = "El monto de la factura ingresada supera el monto de la Orden de Compra.";
                                                    }
                                                    break;
                                                }
                                                else
                                                {
                                                    l.Estado = "El número de PO indicado no se encuentra registrado. Favor validar los datos ingresados.";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            l.Estado = "Debe indicar el número de la factura.";
                                        }
                                    }
                                    else
                                    {
                                        l.Estado = "El monto total de la factura no coincide con la sumatoria del monto y los impuestos.";
                                    }
                                }
                                else
                                {
                                    l.Estado = "El número de factura indicado ya se encuentra en proceso.";
                                }
                            }
                            else
                            {
                                l.Estado = "El código de proveedor no coincide con el usuario.";
                            }


                            l.codigoProveedor = d.codigoProveedor;
                            l.numeroPO = d.numPO;
                            l.numeroFiscal = d.numFiscal;
                            l.consecutivo = c;
                            c++;

                            lista.Add(l);

                        }
                        else
                        {
                            ExcelResultado l = new ExcelResultado();
                            l.codigoProveedor = fila.ItemArray[0].ToString();
                            l.numeroPO = fila.ItemArray[1].ToString();
                            l.numeroFiscal = fila.ItemArray[3].ToString();
                            l.Estado = "No se encontro el archivo indicado, por favor revise los datos ingresados en el excel.";
                            lista.Add(l);

                        }
                    }
                    else
                    {
                    }
                }
                //}


                app = Conexion.DesconectarOnBase(app);
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method CargarMasivamenteDocumentos in DocumentosDM, System message: {e}");
            }
            
            return lista;
        }

        public long ImportarDocumentoConstancia(Documentos model)
        {
            Application app = null;
            long newDocID = -1;

            try
            {
                app = Conexion.ConectarOnBase(app, model.codigoProveedor);

                Stream documentConverted = model.filePathFactura.InputStream;

                using (PageData pageData = app.Core.Storage.CreatePageData(documentConverted, "PDF"))
                {
                    DocumentType docType = app.Core.DocumentTypes.Find(107); // Documento constancia de pago a cuenta

                    FileType imageFileType = app.Core.FileTypes.Find(16);

                    StoreNewDocumentProperties props = app.Core.Storage.CreateStoreNewDocumentProperties(docType, imageFileType);

                    props.AddKeyword(CrearKeyword(app, "Código del proveedor", model.codigoProveedor));
                    props.AddKeyword(CrearKeyword(app, "RTN", model.rtn));
                    props.AddKeyword(CrearKeyword(app, "Fecha de vencimiento", DateTime.Parse(model.fechaDeVencimiento)));


                    Document newDoc = app.Core.Storage.StoreNewDocument(pageData, props);

                    newDocID = newDoc.ID;
                }


                app = Conexion.DesconectarOnBase(app);
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ImportarDocumentoConstancia in DocumentosDM, System message: {e}");
            }
            
            return newDocID;
        }

        private bool ValidarNumeroMigo(string codigoProveedor, string numPO, string numMigo, string migosWS)
        {
            bool resultado = false;
            
            try
            {
                string[] migos = migosWS.Split('l');

                foreach (string a in migos)
                {
                    if (a != "" && a == numMigo)
                    {
                        resultado = true;
                        break;
                    }
                }

            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ValidarNumeroMigo in DocumentosDM, System message: {e}");
            }
               
            return resultado;
            
        }

        //public List<Documentos> ConsultaDocumentoFactura(string codigoProveedor)
        //{
        //    List<Documentos> lista = new List<Documentos>();

        //    Application app = null;
        //    app = Conexion.ConectarOnBase(app);
        //    DocumentList documents = null;
        //    int i = 1;

        //    DocumentType docType = app.Core.DocumentTypes.Find(363); //factura
        //    if (docType == null)
        //    {
        //        throw new Exception("Documento no encontrado");
        //    }

        //    DocumentQuery document = app.Core.CreateDocumentQuery();
        //    document.AddDocumentType(docType);

        //    if (codigoProveedor != "admin")
        //    {
        //        document.AddKeyword("Código proveedor", codigoProveedor, KeywordOperator.Equal, KeywordRelation.Or);
        //    }
        //    document.AddKeyword("Estado", "NO", KeywordOperator.Equal, KeywordRelation.Or);

        //    documents = document.Execute(100);

        //    foreach (Document doc in documents)
        //    {
        //        // doc.KeywordRecords.
        //        Document documento = app.Core.GetDocumentByID(doc.ID);

        //        if (documento != null)
        //        {
        //            Documentos d = new Documentos();
        //            d.idPO = doc.ID;
        //            KeywordRecordList keyRecList = documento.KeywordRecords;

        //            foreach (KeywordRecord rec in keyRecList)
        //            {

        //                foreach (Keyword key in rec.Keywords)
        //                {
        //                    if (key.KeywordType.Name == "Código proveedor")
        //                    {
        //                        d.codigoProveedor = key.AlphaNumericValue;
        //                    }
        //                    if (key.KeywordType.Name == "Número de PO")
        //                    {
        //                        d.numPO = key.AlphaNumericValue;
        //                    }
        //                    if (key.KeywordType.Name == "Número fiscal")
        //                    {
        //                        d.numFiscal = key.AlphaNumericValue;
        //                    }
        //                }
        //                d.consecutivo = i;
        //                var obj = bd.ConsultarNumMigo(d.numPO, d.codigoProveedor);
        //                foreach (var o in obj)
        //                {
        //                    d.numMigo = o.NumeroMigo;
        //                }
        //                i++;

        //            }

        //            lista.Add(d);
        //        }

        //    }

        //    app = Conexion.DesconectarOnBase(app);

        //    return lista;
        //}

        public List<Documentos> ConsultarDocumentosPOyFacturas(string usuario)
        {
            List<Documentos> lista = new List<Documentos>();
            int consecutivo = 1;
            Application app = null;

            try
            {
                app = Conexion.ConectarOnBase(app, usuario);

                // CustomQuery consulta = app.Core.CustomQueries.Find(101);
                var obj = ob.ConsultarFacturasPorProveedor(usuario);

                foreach (var o in obj)
                {
                    Documentos d = new Documentos();
                    Document documento = app.Core.GetDocumentByID(o.ID);
                    d.consecutivo = consecutivo;
                    d.codigoProveedor = o.CodigoProveedor;
                    d.numPO = o.OC;
                    if (o.MontoTotal != null)
                        d.montoTotal = o.MontoTotal.Value.ToString();
                    d.numFiscal = o.NumFactura;
                    d.estado = o.Estado;
                    DefaultDataProvider defaultDataProvider = app.Core.Retrieval.Default;
                    // Use a using statement to get the PageData (for cleanup), and then save the document to disk. To Save, use the Save method above.
                    using (PageData pageData = defaultDataProvider.GetDocument(documento.DefaultRenditionOfLatestRevision))
                    {
                        Stream stream = pageData.Stream;

                        byte[] result;
                        using (var streamReader = new MemoryStream())
                        {
                            stream.CopyTo(streamReader);
                            result = streamReader.ToArray();
                        }

                        string docOB = Convert.ToBase64String(result);

                        // d.documentoPO = "data:application/pdf;base64," + docOB;
                        d.documentoPO = "data:application/pdf;base64," + docOB;
                    }

                    lista.Add(d);
                    consecutivo++;
                }
                //}

                app = Conexion.DesconectarOnBase(app);
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ConsultarDocumentosPOyFacturas in DocumentosDM, System message: {e}");
            }       
            
            return lista;
        }

        public List<Documentos> ConsultarDocumentosPDFOrdenCompra(string usuario, List<Documentos> lista)
        {
            try
            {
                List<Documentos> lista2 = new List<Documentos>();
                int consecutivo = 1;
                Application app = null;
                app = Conexion.ConectarOnBase(app, usuario);

                //CustomQuery consulta = app.Core.CustomQueries.Find(105);
                var obj = ob.ConsultarOrdenesCompraPorProveedor(usuario);

                //if (consulta != null)
                //{
                //    DocumentQuery docQ = app.Core.CreateDocumentQuery();

                //    docQ.AddCustomQuery(consulta);

                //    if (usuario != "admin")
                //    {
                //        docQ.AddKeyword("Código del proveedor", usuario, KeywordOperator.Equal, KeywordRelation.Or);
                //    }

                //    // Execute the Query
                //    QueryResult queryResult = docQ.ExecuteQueryResults(100);

                //    foreach (QueryResultItem resultItem in queryResult.QueryResultItems)
                foreach (var o in obj)
                {
                    Documentos d = new Documentos();

                    Document documento = app.Core.GetDocumentByID(o.ID);
                    //Document documento = app.Core.GetDocumentByID(resultItem.Document.ID);

                    d.consecutivo = consecutivo;
                    d.codigoProveedor = o.CodigoProveedor;
                    //d.codigoProveedor = resultItem.DisplayColumns[0].ToString();
                    d.numPO = o.OC;
                    //d.numPO = resultItem.DisplayColumns[1].ToString();

                    //DefaultDataProvider defaultDataProvider = app.Core.Retrieval.Default;
                    //// Use a using statement to get the PageData (for cleanup), and then save the document to disk. To Save, use the Save method above.
                    //using (PageData pageData = defaultDataProvider.GetDocument(documento.DefaultRenditionOfLatestRevision))
                    //{
                    //    Stream stream = pageData.Stream;

                    //    byte[] result;
                    //    using (var streamReader = new MemoryStream())
                    //    {
                    //        stream.CopyTo(streamReader);
                    //        result = streamReader.ToArray();
                    //    }

                    //    string docOB = Convert.ToBase64String(result);

                    //    d.documentoPO = "data:application/pdf;base64," + docOB;
                    //}

                    lista2.Add(d);
                    consecutivo++;
                    //}
                }

                app = Conexion.DesconectarOnBase(app);


                foreach (Documentos d in lista)
                {
                    foreach (Documentos f in lista2)
                    {
                        if (d.numPO == f.numPO)
                        {
                            d.documentoPO = f.documentoPO;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ConsultarDocumentosPDFOrdenCompra in DocumentosDM, System message: {e}");
            }
            
            return lista;
        }

        private DateTime ConsultaFechaConstanciaPagoCuenta(string proveedor)
        {
            DateTime fechaVencimiento = new DateTime(1900, 10, 17);
            var obj = ob.ConsultarConstanciaPagoCuenta(proveedor);
            foreach (var o in obj)
            {
                fechaVencimiento = o.FechaVencimiento.Value;
            }

            return fechaVencimiento;
        }

        /// <summary>
        /// Consulta si el proveedor tiene constancia de cuenta a pago y envía el msj de advertencia en caso de que no tenga.
        /// </summary>
        /// <param name="proveedor"></param>
        /// <returns></returns>
        public string ConsultaConstanciaPagoCuenta(string proveedor)
        {
            string resultado = null;

            try
            {
                DateTime fechaError = new DateTime(1900, 10, 17);
                DateTime fecha = ConsultaFechaConstanciaPagoCuenta(proveedor);

                if (fecha != fechaError)
                {
                    DateTime fechaFin = DateTime.Now.AddDays(15);

                    if (fecha < DateTime.Now)//Esta vencida
                    {
                        resultado = "Estimado proveedor, su constancia de pago a cuenta se encuentra vencida, por lo que se le aplicará la retención del impuesto sobre la renta.";
                    }
                    else if (fecha <= fechaFin) // Esta por vencer
                    {
                        resultado = "Estimado proveedor, su constancia de pago a cuenta esta por vencer. Por favor adjunte el documento actualizado en el portal.";
                    }
                }
                else
                {
                    resultado = "Estimado proveedor, no cuenta con una constancia de pago a cuenta en el sistema, por lo que se le aplicará la retención del impuesto sobre la renta.";
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ConsultaConstanciaPagoCuenta in DocumentosDM, System message: {e}");
            }
            
            return resultado;
        }

        public string ConsultarDocumento(long id, string codigoProveedor)
        {
            string resp = string.Empty;
            
            try
            {
                Application app = null;
                app = Conexion.ConectarOnBase(app, codigoProveedor);
                Document documento = app.Core.GetDocumentByID(id);
                DefaultDataProvider defaultDataProvider = app.Core.Retrieval.Default;
                // Use a using statement to get the PageData (for cleanup), and then save the document to disk. To Save, use the Save method above.
                using (PageData pageData = defaultDataProvider.GetDocument(documento.DefaultRenditionOfLatestRevision))
                {
                    Stream stream = pageData.Stream;

                    byte[] result;
                    using (var streamReader = new MemoryStream())
                    {
                        stream.CopyTo(streamReader);
                        result = streamReader.ToArray();
                    }

                    string docOB = Convert.ToBase64String(result);

                    resp = "data:application/pdf;base64," + docOB;
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ConsultarDocumento in DocumentosDM, System message: {e}");
            }
            
            return resp;
        }

        public List<Documentos> ConsultarEstadoCuenta(string usuario)
        {
            List<Documentos> lista = new List<Documentos>();
            
            try
            {
                List<Documentos> lista2 = new List<Documentos>();
                int consecutivo = 1;

                Documentos doc = new Documentos();

                var obj = ob.ConsultarEstadoDeCuenta(usuario);

                foreach (var o in obj)
                {
                    Documentos d = new Documentos();

                    //Document documento = app.Core.GetDocumentByID(o.DocumentHandle);
                    d.consecutivo = consecutivo;
                    d.codigoProveedor = o.CodigoProveedor;
                    d.numPO = o.NumeroOC;
                    if (o.Monto != null)
                    {
                        d.montoTotal = string.Format("{0:0,0.0}", o.Monto.Value);
                        d.montoTotalMostrar = o.Monto.Value.ToString();
                    }
                    else
                    {
                        d.montoTotal = "0";
                        d.montoTotalMostrar = "0";
                    }
                    d.numFiscal = o.NumeroFactura;
                    d.estado = o.Estado;
                    d.moneda = o.Moneda;
                    d.guiEstatus = o.GuiEstatus;



                    lista.Add(d);
                    consecutivo++;
                }

                foreach (Documentos g in lista)
                {
                    if (g.estado.Equals("FACTURA PAGADA."))
                    {
                        var nuevaLista = lista.FindAll(p => p.guiEstatus == g.guiEstatus);
                        Documentos d = new Documentos();
                        foreach (var o in nuevaLista)
                        {

                            d.consecutivo = consecutivo;
                            d.codigoProveedor = o.codigoProveedor;
                            d.montoTotal = o.montoTotal;
                            d.montoTotalMostrar = o.montoTotalMostrar;
                            d.numPO = o.numPO + ". " + d.numPO;
                            d.numFiscal = o.numFiscal + ". " + d.numFiscal;
                            d.estado = o.estado;
                            d.moneda = o.moneda;
                            d.guiEstatus = o.guiEstatus;
                        }
                        lista2.Add(d);
                        consecutivo++;
                    }
                }

                lista.RemoveAll(t => t.estado == "FACTURA PAGADA.");

                foreach (var i in lista2)
                {
                    var buscar = lista.Find(p => p.guiEstatus == i.guiEstatus);
                    if (buscar == null)
                    {
                        lista.Add(i);
                    }
                }


                float montoRecibidas = 0;
                float montoEnEsperaFisico = 0;
                float montoPendientePago = 0;
                float montoFacturaPagadas = 0;
                float montoRechazados = 0;
                float montoRevisionMontos = 0;
                float montoTotal = 0;
                float montoTotalProceso = 0;
                float montoAnalisisContable = 0;
                float montoSumaACyPorPagar = 0;

                float USDmontoRecibidas = 0;
                float USDmontoEnEsperaFisico = 0;
                float USDmontoPendientePago = 0;
                float USDmontoFacturaPagadas = 0;
                float USDmontoRechazados = 0;
                float USDmontoRevisionMontos = 0;
                float USDmontoTotal = 0;
                float USDmontoTotalProceso = 0;
                float USDmontoAnalisisContable = 0;
                float USDmontoSumaACyPorPagar = 0;


                foreach (Documentos d in lista)
                {
                    if (d.moneda.Equals("HNL"))
                    {
                        float montoF = float.Parse(d.montoTotalMostrar);
                        switch (d.estado)
                        {
                            case "EN ESPERA DE VALIDACION DE LA FACTURA": /*Flujo Validar Factura*/
                                montoRecibidas = montoF + montoRecibidas;
                                montoAnalisisContable = montoAnalisisContable + montoF;
                                montoSumaACyPorPagar = montoSumaACyPorPagar + montoF;
                                break;

                            case "EN ESPERA DE VERIFICACION DE FACTURA FISICA": /*Flujo Verificacion de factura en fisico*/
                                montoEnEsperaFisico = montoF + montoEnEsperaFisico;
                                montoAnalisisContable = montoAnalisisContable + montoF;
                                montoSumaACyPorPagar = montoSumaACyPorPagar + montoF;
                                break;
                            case "FACTURA FISICA RECHAZADA": /*Flujo factura física rechazada*/
                                montoEnEsperaFisico = montoF + montoEnEsperaFisico;
                                montoAnalisisContable = montoAnalisisContable + montoF;
                                montoSumaACyPorPagar = montoSumaACyPorPagar + montoF;
                                break;
                            case "EN ESPERA DE PAGO.": /* Flujo Monitoreo de Facturas */
                                montoPendientePago = montoF + montoPendientePago;
                                montoSumaACyPorPagar = montoSumaACyPorPagar + montoF;
                                break;
                            case "FACTURA PAGADA.": /*Flujo Facturas aprobadas para pago */
                                montoFacturaPagadas = montoF + montoFacturaPagadas;
                                break;
                            case "FACTURA RECHAZADA": /*Flujo Facturas Rechazadas*/
                                montoRechazados = montoF + montoRechazados;
                                break;
                            default:
                                break;
                        }

                        if (d.estado != null && (d.estado.Equals("EN ESPERA DE VALIDACION DE LA FACTURA") ||
                            d.estado.Equals("EN ESPERA DE VERIFICACION DE FACTURA FISICA") ||
                            d.estado.Equals("FACTURA FISICA RECHAZADA")))
                        {
                            montoTotalProceso = montoTotalProceso + montoF;
                        }

                        if (d.estado != null && !d.estado.Equals("FACTURA RECHAZADA"))
                        {
                            montoTotal = montoTotal + montoF;
                        }
                    }
                    else
                    {
                        float USDmontoF = float.Parse(d.montoTotalMostrar);
                        switch (d.estado)
                        {
                            case "EN ESPERA DE VALIDACION DE LA FACTURA": /*Flujo Validar Factura*/
                                USDmontoRecibidas = USDmontoF + USDmontoRecibidas;
                                USDmontoAnalisisContable = USDmontoAnalisisContable + USDmontoF;
                                USDmontoSumaACyPorPagar = USDmontoSumaACyPorPagar + USDmontoF;
                                break;

                            case "EN ESPERA DE VERIFICACION DE FACTURA FISICA": /*Flujo Verificacion de factura en fisico*/
                                USDmontoEnEsperaFisico = USDmontoF + USDmontoEnEsperaFisico;
                                USDmontoAnalisisContable = USDmontoAnalisisContable + USDmontoF;
                                USDmontoSumaACyPorPagar = USDmontoSumaACyPorPagar + USDmontoF;
                                break;
                            case "FACTURA FISICA RECHAZADA": /*Flujo factura física rechazada*/
                                USDmontoEnEsperaFisico = USDmontoF + USDmontoEnEsperaFisico;
                                USDmontoAnalisisContable = USDmontoAnalisisContable + USDmontoF;
                                USDmontoSumaACyPorPagar = USDmontoSumaACyPorPagar + USDmontoF;
                                break;
                            case "EN ESPERA DE PAGO.": /* Flujo Monitoreo de Facturas */
                                USDmontoPendientePago = USDmontoF + USDmontoPendientePago;
                                USDmontoSumaACyPorPagar = USDmontoSumaACyPorPagar + USDmontoF;
                                break;
                            case "FACTURA PAGADA.": /*Flujo Facturas aprobadas para pago */
                                USDmontoFacturaPagadas = USDmontoF + USDmontoFacturaPagadas;
                                break;
                            case "FACTURA RECHAZADA": /*Flujo Facturas Rechazadas*/
                                USDmontoRechazados = USDmontoF + USDmontoRechazados;
                                break;
                            default:
                                break;
                        }

                        if (d.estado != null && (d.estado.Equals("EN ESPERA DE VALIDACION DE LA FACTURA") ||
                            d.estado.Equals("EN ESPERA DE VERIFICACION DE FACTURA FISICA") ||
                            d.estado.Equals("FACTURA FISICA RECHAZADA")))
                        {
                            USDmontoTotalProceso = USDmontoTotalProceso + USDmontoF;
                        }

                        if (d.estado != null && !d.estado.Equals("FACTURA RECHAZADA"))
                        {
                            USDmontoTotal = USDmontoTotal + USDmontoF;
                        }
                    }
                }

                if (lista.Count > 0)
                {
                    lista[0].montoRecibidas = string.Format("{0:0,0.0}", montoAnalisisContable);
                    lista[0].montoEnEsperaFisico = string.Format("{0:0,0.0}", montoSumaACyPorPagar);
                    lista[0].montoPendientePago = string.Format("{0:0,0.0}", montoPendientePago);
                    lista[0].montoFacturaPagadas = string.Format("{0:0,0.0}", montoFacturaPagadas);
                    lista[0].montoRechazados = string.Format("{0:0,0.0}", montoRechazados);
                    lista[0].montoRevisionMontos = string.Format("{0:0,0.0}", montoRevisionMontos);
                    lista[0].montoTotalPagado = string.Format("{0:0,0.0}", montoTotal);
                    lista[0].montoTotalProceso = string.Format("{0:0,0.0}", montoTotalProceso);

                    lista[0].USDmontoRecibidas = string.Format("{0:0,0.0}", USDmontoAnalisisContable);
                    lista[0].USDmontoEnEsperaFisico = string.Format("{0:0,0.0}", USDmontoSumaACyPorPagar);
                    lista[0].USDmontoPendientePago = string.Format("{0:0,0.0}", USDmontoPendientePago);
                    lista[0].USDmontoFacturaPagadas = string.Format("{0:0,0.0}", USDmontoFacturaPagadas);
                    lista[0].USDmontoRechazados = string.Format("{0:0,0.0}", USDmontoRechazados);
                    lista[0].USDmontoRevisionMontos = string.Format("{0:0,0.0}", USDmontoRevisionMontos);
                    lista[0].USDmontoTotalPagado = string.Format("{0:0,0.0}", USDmontoTotal);
                    lista[0].USDmontoTotalProceso = string.Format("{0:0,0.0}", USDmontoTotalProceso);
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ConsultarEstadoCuenta in DocumentosDM, System message: {e}");
            }
            
            return lista;
        }

        public List<Documentos> ConsultarEstadoCuentaConFecha(string usuario, DateTime fechaInicio, DateTime fechaFin)
        {
            List<Documentos> lista = new List<Documentos>();
            
            try
            {
                int consecutivo = 1;

                Documentos doc = new Documentos();

                var obj = ob.ConsultarEstadoDeCuentaConFechas(usuario, fechaInicio, fechaFin);

                foreach (var o in obj)
                {
                    Documentos d = new Documentos();

                    //Document documento = app.Core.GetDocumentByID(o.DocumentHandle);
                    d.consecutivo = consecutivo;
                    d.codigoProveedor = o.CodigoProveedor;
                    d.numPO = o.NumeroOC;
                    d.montoTotal = string.Format("{0:0,0.0}", o.Monto.Value);
                    d.montoTotalMostrar = o.Monto.Value.ToString();
                    d.numFiscal = o.NumeroFactura;
                    d.estado = o.Estado;


                    lista.Add(d);
                    consecutivo++;
                }


                float montoRecibidas = 0;
                float montoEnEsperaFisico = 0;
                float montoPendientePago = 0;
                float montoFacturaPagadas = 0;
                float montoRechazados = 0;
                float montoRevisionMontos = 0;
                float montoTotal = 0;
                float montoTotalProceso = 0;
                float montoAnalisisContable = 0;
                float montoSumaACyPorPagar = 0;


                foreach (Documentos d in lista)
                {
                    float montoF = float.Parse(d.montoTotalMostrar);
                    switch (d.estado)
                    {
                        case "EN ESPERA DE VALIDACION DE LA FACTURA": /*Flujo Validar Factura*/
                            montoRecibidas = montoF + montoRecibidas;
                            montoAnalisisContable = montoAnalisisContable + montoF;
                            montoSumaACyPorPagar = montoSumaACyPorPagar + montoF;
                            break;

                        case "EN ESPERA DE VERIFICACION DE FACTURA FISICA": /*Flujo Verificacion de factura en fisico*/
                            montoEnEsperaFisico = montoF + montoEnEsperaFisico;
                            montoAnalisisContable = montoAnalisisContable + montoF;
                            montoSumaACyPorPagar = montoSumaACyPorPagar + montoF;
                            break;
                        case "FACTURA FISICA RECHAZADA": /*Flujo factura física rechazada*/
                            montoEnEsperaFisico = montoF + montoEnEsperaFisico;
                            montoAnalisisContable = montoAnalisisContable + montoF;
                            montoSumaACyPorPagar = montoSumaACyPorPagar + montoF;
                            break;
                        case "EN ESPERA DE PAGO.": /* Flujo Monitoreo de Facturas */
                            montoPendientePago = montoF + montoPendientePago;
                            montoSumaACyPorPagar = montoSumaACyPorPagar + montoF;
                            break;
                        case "FACTURA PAGADA": /*Flujo Facturas aprobadas para pago */
                            montoFacturaPagadas = montoF + montoFacturaPagadas;
                            break;
                        case "FACTURA RECHAZADA": /*Flujo Facturas Rechazadas*/
                            montoRechazados = montoF + montoRechazados;
                            break;
                        default:
                            break;
                    }

                    if (d.estado != null && (d.estado.Equals("EN ESPERA DE VALIDACION DE LA FACTURA") ||
                        d.estado.Equals("EN ESPERA DE VERIFICACION DE FACTURA FISICA") ||
                        d.estado.Equals("FACTURA FISICA RECHAZADA")))
                    {
                        montoTotalProceso = montoTotalProceso + montoF;
                    }

                    if (d.estado != null && !d.estado.Equals("FACTURA RECHAZADA"))
                    {
                        montoTotal = montoTotal + montoF;
                    }
                }

                if (lista.Count > 0)
                {
                    lista[0].montoRecibidas = string.Format("{0:0,0.0}", montoAnalisisContable);
                    lista[0].montoEnEsperaFisico = string.Format("{0:0,0.0}", montoSumaACyPorPagar);
                    lista[0].montoPendientePago = string.Format("{0:0,0.0}", montoPendientePago);
                    lista[0].montoFacturaPagadas = string.Format("{0:0,0.0}", montoFacturaPagadas);
                    lista[0].montoRechazados = string.Format("{0:0,0.0}", montoRechazados);
                    lista[0].montoRevisionMontos = string.Format("{0:0,0.0}", montoRevisionMontos);
                    lista[0].montoTotalPagado = string.Format("{0:0,0.0}", montoTotal);
                    lista[0].montoTotalProceso = string.Format("{0:0,0.0}", montoTotalProceso);
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ConsultarEstadoCuentaConFecha in DocumentosDM, System message: {e}");
            }

            return lista;
        }
    }
}