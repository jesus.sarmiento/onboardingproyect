﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Portal_Proveedor.Services.Utility;

namespace Portal_Proveedor.Models.OnBase
{
    public class CargaExcelDM
    {

        public DataTable ObtenerDatos(Stream MyExcelStream, bool ReadOnly)
        {
            DataTable dt = new DataTable();
            
            try
            {
                using (SpreadsheetDocument sDoc = SpreadsheetDocument.Open(MyExcelStream, ReadOnly))
                {
                    WorkbookPart workbookPart = sDoc.WorkbookPart;
                    IEnumerable<Sheet> sheets = sDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)sDoc.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        dt.Columns.Add(ObtenerValorCelda(sDoc, cell));
                    }

                    foreach (Row row in rows) //this will also include your header row...
                    {
                        DataRow tempRow = dt.NewRow();

                        for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                        {
                            tempRow[i] = ObtenerValorCelda(sDoc, row.Descendants<Cell>().ElementAt(i));
                        }

                        dt.Rows.Add(tempRow);
                    }
                }
                //dt.Rows[0].Table.
                dt.Rows.RemoveAt(0);
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ObtenerDatos in CargaExcelDM, System message: {e}");
            }
            
            return dt;
        }

        private static string ObtenerValorCelda(SpreadsheetDocument document, Cell cell)
        {
            var response = string.Empty;
            try
            {
                DocumentFormat.OpenXml.Spreadsheet.CellValue x = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                x.InnerXml = "";

                SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
                if (cell.CellValue == null)
                {
                    cell.CellValue = x;
                }
                string value = cell.CellValue.InnerXml;

                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    response = stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                }
                else
                {
                    response = value;
                }
            }
            catch (Exception e)
            {
                LoggerManager.GetInatance().Error($"Failed method ObtenerValorCelda in CargaExcelDM, System message: {e}");
            }

            return response;
        }
    }
}