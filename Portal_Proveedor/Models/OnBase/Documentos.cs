﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Portal_Proveedor.Models.OnBase
{
    public class Documentos
    {
        public int consecutivo { get; set; }
        public long idPO { get; set; }
        public string codigoProveedor { get; set; }
        public string numPO { get; set; }
        public string numMigo { get; set; }
        public string montoPO { get; set; }
        public string numFiscal { get; set; }
        public string montoSinImpuesto { get; set; }
        public string impuesto { get; set; }
        public string montoTotal { get; set; }
        public string documentoPO { get; set; }
        public HttpPostedFileBase filePathFactura { get; set; }
        public HttpPostedFileBase docSoporte { get; set; }
        public string estado { get; set; }
        public string montoRestante { get; set; }
        public string numeroCAI { get; set; }
        public string fechaDeVencimiento { get; set; }
        public string fechaDeEmision { get; set; }
        public IEnumerable<HttpPostedFileBase> files { get; set; }
   
        public string rtn { get; set; }

        public string tieneConstancia { get; set; }

        public DateTime fi { get; set; }
        public DateTime ff { get; set; }


        public string montoTotalMostrar { get; set; }
        public string montoRecibidas { get; set; }
        public string montoEnEsperaFisico { get; set; }
        public string montoPendientePago { get; set; }
        public string montoFacturaPagadas { get; set; }
        public string montoRechazados { get; set; }
        public string montoRevisionMontos { get; set; }
        public string montoTotalPagado { get; set; }
        public string montoTotalProceso { get; set; }

        public string USDmontoTotalMostrar { get; set; }
        public string USDmontoRecibidas { get; set; }
        public string USDmontoEnEsperaFisico { get; set; }
        public string USDmontoPendientePago { get; set; }
        public string USDmontoFacturaPagadas { get; set; }
        public string USDmontoRechazados { get; set; }
        public string USDmontoRevisionMontos { get; set; }
        public string USDmontoTotalPagado { get; set; }
        public string USDmontoTotalProceso { get; set; }

        public string moneda { get; set; }
        public string guiEstatus { get; set; }
       
    }
}