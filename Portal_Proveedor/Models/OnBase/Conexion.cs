﻿using Hyland.Unity;
using Portal_Proveedor.Services.Utility;
using System;

namespace Portal_Proveedor.Models.OnBase
{
    public class Conexion
    {
        private static Datos.DatosDataContext bd = new Datos.DatosDataContext();

        public static Application ConectarOnBase(Hyland.Unity.Application app, string usuario)
        {

            app = VerificaSessionActiva(app);

            if (app == null || !app.IsConnected)
            {
                app = CrearNuevaConexion(app, usuario);
            }

            return app;

        }

        private static Application CrearNuevaConexion(Hyland.Unity.Application app, string usuario)
        {
            try
            {
                OnBaseAuthenticationProperties props = Application.CreateOnBaseAuthenticationProperties
                                     (
                                      Portal_Proveedor.Properties.Settings.Default.Conexion,
                                     Portal_Proveedor.Properties.Settings.Default.Usuario,
                                     Portal_Proveedor.Properties.Settings.Default.Clave,
                                     Portal_Proveedor.Properties.Settings.Default.ODBC);
                //props.IsDisconnectEnabled = true;
                app = Application.Connect(props);

                bd.AgregarSesionOnBase(app.SessionID, DateTime.Now, usuario,Portal_Proveedor.Properties.Settings.Default.Usuario);
            }
            catch (Exception ex)
            {
                LoggerManager.GetInatance().Error($"Failed method CrearNuevaConexion in ConexionController, System message: {ex}");
                throw new Exception(ex.Message);
            }

            return app;
        }

        private static Application VerificaSessionActiva(Hyland.Unity.Application app)
        {
            string sessionID = ConsultarUltimaSesion();

            if (sessionID != null && sessionID != "")
            {
                SessionIDAuthenticationProperties authProps = Application.CreateSessionIDAuthenticationProperties
                    (
                    Portal_Proveedor.Properties.Settings.Default.Conexion,
                    sessionID,
                    false
                    );
                try
                {
                    app = Application.Connect(authProps);
                }
                catch (Exception ex)
                {
                    //An error occurred within the Unity API: Failed to get session for session id: 469da57f-e20e-4cb7-9011-2eb9765b56d0. Client host address: 192.168.20.58 (192.168.20.58)
                    app = null;
                    LoggerManager.GetInatance().Error($"Failed method VerificaSessionActiva in ConexionController, System message: {ex}");
                }
            }
            return app;
        }

        private static string ConsultarUltimaSesion()
        {
            string SessionID = string.Empty;

            var obj = bd.ConsultarUltimaSessionActivaOnBase();

            foreach (var o in obj)
            {
                SessionID = o.IdSession;
            }

            return SessionID;
        }

        public static Application DesconectarOnBase(Hyland.Unity.Application app)
        {
            try
            {
                if (app != null && app.IsConnected)
                {
                    //app.Dispose();
                }

            }
            catch (Exception ex)
            {
                LoggerManager.GetInatance().Error($"Failed method DesconectarOnBase in ConexionController, System message: {ex}");
                throw new Exception(ex.Message);
            }
            return app;

        }
    }
}